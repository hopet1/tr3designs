import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'package:http/http.dart' as http;
import 'dart:convert' show Encoding, json;
import 'package:tr3designs_flutter/constants.dart';

//add "dimensions" param to Etsy getListings API method to retrieve lwh.

class SpotifyAPI {
  //REQUIRES FRESH ACCESS TOKEN...SO YEA...PROBABLY CANT USE THIS
  static Future<String> getAlbum() {
    final String albumID = '4Wv5UAieM1LDEYVq5WmqDd'; //KOD
    String endpoint = 'https://api.spotify.com/v1/albums/${albumID}';
    return http.get(endpoint, headers: {
      HttpHeaders.authorizationHeader:
          "Bearer BQCNouCkD3zOGv88n_9pyBjU1of91VoVAN1Lba9nE5WgU4BkYMsSMTu0HwZJT3DWcAVWsV4cIonz6mSJhP1gjqux-HFUmlN_fLGJT9-wQKYVgk5vjBD7yc9RZj2w3y6i4t8I_I89qz_k566_TGJAQ9hwCndJm4Wx8ZqqC6AQ_RF4_Bs5CYdGyAR9k6Pa6q4x"
    }).then((res) {
      final String response = res.body;
      final int statusCode = res.statusCode;

      if (statusCode < 200 || statusCode >= 400 || json == null) {
        throw ('Failure');
      }

      return response;
    }).catchError((e) {
      print(
        e.toString(),
      );
    });
  }
}

class FacebookAPI {
  static Future<String> getUser(String userID) {
    String endpoint =
        'https://graph.facebook.com/v3.2/${userID}?fields=name,picture.type(large)&access_token=${FACEBOOK_ACCESS_TOKEN}';
    return http.get(endpoint).then((res) {
      final String response = res.body;
      final int statusCode = res.statusCode;

      if (statusCode < 200 || statusCode >= 400 || json == null) {
        throw ('Failure');
      }

      return response;
    }).catchError((e) {
      print(
        e.toString(),
      );
    });
  }
}

class BloggerAPI {
  static Future<String> getAllPosts() {
    String endpoint =
        'https://www.googleapis.com/blogger/v3/blogs/${BLOGGER_BLOG_ID}/posts?key=${GOOGLE_API_KEY}&fetchImages=true';
    return http.get(endpoint).then((res) {
      final String response = res.body;
      final int statusCode = res.statusCode;

      if (statusCode < 200 || statusCode >= 400 || json == null) {
        throw ('Failure');
      }

      return response;
    }).catchError((e) {
      print(
        e.toString(),
      );
    });
  }
}

class EtsyAPI {
  static final String _baseUrl = 'https://openapi.etsy.com/v2/';
  static final String _etsyApiKey = '2aakmj1v20hfw1xul6ruaenn';

  static Future<String> getAllListings() {
    String endpoint =
        '${_baseUrl}shops/trxdxsxgns/listings/active.js?api_key=${_etsyApiKey}&includes=MainImage&fields=${ETSY_FIELDS}&limit=100';
    return http.get(endpoint).then((res) {
      final String response = res.body;
      final int statusCode = res.statusCode;

      if (statusCode < 200 || statusCode >= 400 || json == null) {
        throw ('Failure');
      }

      return response.substring(
          5,
          response.length -
              2); //Remove extra characters at beginning and end of resopnse. Look into more later.
    }).catchError((e) {
      print(
        e.toString(),
      );
    });
  }

  static Future<String> getAllSections() {
    String endpoint =
        '${_baseUrl}shops/trxdxsxgns/sections?api_key=${_etsyApiKey}';
    return http.get(endpoint).then((res) {
      final String response = res.body;
      final int statusCode = res.statusCode;

      if (statusCode < 200 || statusCode >= 400 || json == null) {
        throw ('Failure');
      }

      return response;
    }).catchError((e) {
      print(
        e.toString(),
      );
    });
  }

  static Future<String> getShopAbout() {
    String endpoint =
        '${_baseUrl}shops/trxdxsxgns/about?api_key=${_etsyApiKey}';
    return http.get(endpoint).then((res) {
      final String response = res.body;
      final int statusCode = res.statusCode;

      if (statusCode < 200 || statusCode >= 400 || json == null) {
        throw ('Failure');
      }

      return response;
    }).catchError((e) {
      print(
        e.toString(),
      );
    });
  }

  static Future<String> getShop() {
    String endpoint = '${_baseUrl}shops/trxdxsxgns?api_key=${_etsyApiKey}';
    return http.get(endpoint).then((res) {
      final String response = res.body;
      final int statusCode = res.statusCode;

      if (statusCode < 200 || statusCode >= 400 || json == null) {
        throw ('Failure');
      }

      return response;
    }).catchError((e) {
      print(
        e.toString(),
      );
    });
  }

  static Future<String> getListingsBySection(int sectionID) {
    String endpoint =
        '${_baseUrl}shops/trxdxsxgns/sections/${sectionID}/listings/active?api_key=${_etsyApiKey}&includes=MainImage&fields=${ETSY_FIELDS}&limit=100';
    return http.get(endpoint).then((res) {
      final String response = res.body;
      final int statusCode = res.statusCode;

      if (statusCode < 200 || statusCode >= 400 || json == null) {
        throw ('Failure');
      }

      return response;
    }).catchError((e) {
      print(
        e.toString(),
      );
    });
  }

  static Future<String> getListingsImages(int listingId) {
    String endpoint =
        '${_baseUrl}listings/${listingId}/images?api_key=${_etsyApiKey}';
    return http.get(endpoint).then((res) {
      final String response = res.body;
      final int statusCode = res.statusCode;

      print(response);

      if (statusCode < 200 || statusCode >= 400 || json == null) {
        throw ('Failure');
      }

      return response; //Remove extra characters at beginning and end of resopnse. Look into more later.
    }).catchError((e) {
      print(
        e.toString(),
      );
    });
  }

  static Future<String> getFeedback() {
    String endpoint =
        '${_baseUrl}users/reygvacb/feedback/as-seller?api_key=${_etsyApiKey}';
    return http.get(endpoint).then((res) {
      final String response = res.body;
      final int statusCode = res.statusCode;

      print(response);

      if (statusCode < 200 || statusCode >= 400 || json == null) {
        throw ('Failure');
      }

      return response; //Remove extra characters at beginning and end of resopnse. Look into more later.
    }).catchError((e) {
      print(
        e.toString(),
      );
    });
  }
}

class YouTubeAPI {
  static Future<String> getVideos(int count) {
    String endpoint =
        'https://www.googleapis.com/youtube/v3/search?key=${GOOGLE_API_KEY}&channelId=${YOUTUBE_CHANNEL_ID}&part=snippet,id&type=video&order=date&maxResults=${count}';
    return http.get(endpoint).then((res) {
      final String response = res.body;
      final int statusCode = res.statusCode;

      if (statusCode < 200 || statusCode >= 400 || json == null) {
        throw ('Failure');
      }

      return response;
    }).catchError((e) {
      print(
        e.toString(),
      );
    });
  }
}

class InstagramAPI {
  static Future<String> getMedia(int count) {
    String endpoint =
        'https://api.instagram.com/v1/users/self/media/recent/?access_token=${IG_API_ACCESS_TOKEN}&count=${count}';
    return http.get(endpoint).then((res) {
      final String response = res.body;
      final int statusCode = res.statusCode;

      if (statusCode < 200 || statusCode >= 400 || json == null) {
        throw ('Failure');
      }

      return response;
    }).catchError((e) {
      print(
        e.toString(),
      );
    });
  }
}

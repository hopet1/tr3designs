import 'package:animator/animator.dart';
import 'package:flutter/material.dart';

/* 
  For more information, go to the package.
  https://medium.com/flutter-community/flutter-animation-has-never-been-easier-part-1-e378e82b2508

  0 cycles/repeats = infinite
*/

class Animations {
  static Animator opacity(Widget widget) {
    return Animator(
      duration: Duration(seconds: 2),
      cycles: 0,
      builder: (anim) => Opacity(
            opacity: anim.value,
            child: widget,
          ),
    );
  }

  // An oscillating curve that grows and then shrinks in magnitude while overshooting its bounds.
  static Animator elasticInOut(Widget widget, int cycles) {
    return Animator(
      duration: Duration(seconds: 1),
      tween: Tween<double>(begin: 0.8, end: 1.4),
      curve: Curves.elasticInOut,
      cycles: cycles,
      builder: (anim) => Transform.scale(
            scale: anim.value,
            child: widget,
          ),
    );
  }

  // A curve that starts quickly and eases into its final position.
  // Over the course of the animation, the object spends more time near its final destination. As a result, the user isn’t left waiting for the animation to finish, and the negative effects of motion are minimized.
  static Animator fastOutSlowIn(Widget widget, int cycles) {
    return Animator(
      tween: Tween<double>(begin: 0.8, end: 1.4),
      curve: Curves.fastOutSlowIn,
      cycles: cycles,
      builder: (anim) => Transform.scale(
            scale: anim.value,
            child: widget,
          ),
    );
  }
}

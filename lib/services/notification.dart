import 'dart:async';
import 'dart:convert';
import 'package:http/http.dart' as http;
import 'dart:convert' show Encoding, json;
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:tr3designs_flutter/constants.dart';

class MyNotification {
  FirebaseMessaging _firebaseMessaging = FirebaseMessaging();

  static Future<bool> _sendNotification(
      String to, String title, String body) async {
    final data = {
      'to': to,
      'priority': 'high',
      'notification': {'title': title, 'body': body},
      'content_available': true
    };
    final headers = {
      'content-type': 'application/json',
      'Authorization': 'key=' + GOOGLE_FCM_SERVER_KEY
    };
    final response = await http.post(
      GOOGLE_FCM_ENDPOINT,
      body: json.encode(data),
      headers: headers,
      encoding: Encoding.getByName('utf-8'),
    );
    if (response.statusCode == 200) {
      return true;
    } else {
      return false;
    }
  }

  void unsubscribeFromTopic(String topic) {
    return this._firebaseMessaging.unsubscribeFromTopic(topic);
  }

  void subscribeToTopic(String topic) {
    return this._firebaseMessaging.subscribeToTopic(topic);
  }

  static Future<bool> sendNotificationToUser(
      String fcmToken, String title, String body) async {
    return await _sendNotification(fcmToken, title, body);
  }

  // static Future<void> sendNotificationToGroup(String group, String title, String body) {
  //   return _sendNotification('/topics/' + group, title, body);
  // }
}

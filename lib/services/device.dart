import 'dart:io' show Platform;

class Device {
  //Return true if Android, false if iOS.
  static bool isAndroid() {
    return Platform.isAndroid;
  }
}

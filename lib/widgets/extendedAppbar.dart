// import 'package:flutter/material.dart';
// import 'package:tr3designs_flutter/constants.dart';

// class ExtendedAppbar {
//   String _title;
//   String _subtitle;
//   List<IconButton> _iconButtons = [];

//   void buildAppBar(String title, [String subtitle, List<IconButton> iconButtons]){
//     this._title = title;
//     this._subtitle = subtitle;
//     this._iconButtons = iconButtons;
//   }

//   Widget exAppbar (){
//     return SliverAppBar(
//       actions: this._iconButtons,
//       expandedHeight: EXPANDED_APPBAR_HEIGHT,
//       flexibleSpace: FlexibleSpaceBar(
//         centerTitle: true,
//         title: Text(this._title),
//         background: Image(
//           image: AssetImage('assets/images/shop_background.jpg'),
//           fit: BoxFit.cover
//         ),

//       ),
//     );
//   }

//   Widget subExAppbar (){
//     return SliverToBoxAdapter(
//       child: Container(
//         color: Colors.white,
//         height: 40,
//         child: Center(
//           child: Text(this._subtitle, style: TextStyle(color: Colors.black, fontWeight: FontWeight.bold)),
//         ),
//       )
//     );
//   }
// }

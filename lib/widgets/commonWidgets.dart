import 'package:flutter/material.dart';
import 'package:tr3designs_flutter/services/animator.dart';

ListTile listTile(
    IconData iconData, Color iconColor, String title, String text) {
  return ListTile(
    leading: Animations.fastOutSlowIn(
        Icon(
          iconData,
          color: iconColor,
        ),
        0),
    title: Text(
      title,
      style: TextStyle(letterSpacing: 2.0, fontWeight: FontWeight.bold),
    ),
    subtitle: Text('\n' + text),
  );
}

ListTile listTileWithAction(IconData iconData, Color iconColor, String title,
    String text, Function() function) {
  return ListTile(
    leading: Animations.fastOutSlowIn(
        Icon(
          iconData,
          color: iconColor,
        ),
        0),
    title: Text(
      title,
      style: TextStyle(letterSpacing: 2.0, fontWeight: FontWeight.bold),
    ),
    subtitle: Text('\n' + text),
    trailing: Icon(Icons.chevron_right),
    onTap: function,
  );
}

import 'package:flutter/material.dart';

//Input Fields
class InputField extends StatelessWidget {
  final dynamic initialValue;
  final TextEditingController controller;
  final IconData icon;
  final String hintText;
  final String labelText;
  final TextInputType textInputType;
  final Color textFieldColor, iconColor;
  final bool obscureText;
  final double bottomMargin;
  final TextStyle textStyle, hintStyle;
  final dynamic validateFunction;
  final dynamic onSaved;
  final Key key;
  final int maxLength;

  InputField(
      {this.initialValue,
      this.controller,
      this.key,
      this.hintText,
      this.labelText,
      this.obscureText,
      this.textInputType,
      this.textFieldColor,
      this.icon,
      this.iconColor,
      this.bottomMargin,
      this.textStyle,
      this.validateFunction,
      this.onSaved,
      this.hintStyle,
      this.maxLength});

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return (Container(
      margin: EdgeInsets.only(bottom: bottomMargin),
      child: DecoratedBox(
        decoration: BoxDecoration(
            borderRadius: BorderRadius.all(
              Radius.circular(30.0),
            ),
            color: textFieldColor),
        child: TextFormField(
          initialValue: initialValue,
          controller: controller,
          style: textStyle,
          key: key,
          obscureText: obscureText,
          keyboardType: textInputType,
          validator: validateFunction,
          onSaved: onSaved,
          maxLength: maxLength,
          decoration: InputDecoration(
            labelText: labelText,
            fillColor: Colors.white,
            hintText: hintText,
            hintStyle: hintStyle,
            icon: this.icon == null
                ? null
                : Icon(
                    icon,
                    color: iconColor,
                  ),
          ),
        ),
      ),
    ));
  }
}

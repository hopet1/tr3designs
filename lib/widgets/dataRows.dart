import 'package:flutter/material.dart';
import 'package:tr3designs_flutter/models/instagramPost.dart';
import 'package:tr3designs_flutter/models/bloggerPost.dart';
import 'package:tr3designs_flutter/models/user.dart';
import 'package:tr3designs_flutter/models/youTubeVideo.dart';
import 'package:tr3designs_flutter/constants.dart';
import 'package:tr3designs_flutter/pages/postDetails.dart';
import 'package:timeago/timeago.dart' as timeago;
import 'package:tr3designs_flutter/services/modal.dart';
import 'package:tr3designs_flutter/services/urlLauncher.dart';
// import 'package:flutter_youtube_view/flutter_youtube_view.dart';
import 'package:html_unescape/html_unescape.dart';

final HtmlUnescape htmlUnescape = HtmlUnescape();

Widget filamentChips(BuildContext context) {
  // List<Chip> chips = List<Chip>();
  // filaments.forEach((filament){
  //   chips.add(Chip(
  //       avatar: CircleAvatar(backgroundImage: NetworkImage(filament.photoUrl),),
  //       label: Text(filament.name),
  //     )
  //   );
  // });

  List<GestureDetector> gds = List<GestureDetector>();
  filaments.forEach((filament) {
    gds.add(
      GestureDetector(
        child: Chip(
          avatar: CircleAvatar(
            backgroundImage: NetworkImage(filament.photoUrl),
          ),
          label: Text(filament.name),
          labelPadding: EdgeInsets.all(10),
        ),
        onTap: () {
          Navigator.of(context).push(
            MaterialPageRoute(
              builder: (ctx) => Scaffold(
                appBar: AppBar(
                  centerTitle: true,
                  title: Text(filament.name),
                ),
                body: Container(
                  color: Colors.black,
                  child: GestureDetector(
                    onTap: () {
                      Navigator.pop(context);
                    },
                    child: Center(
                      child: Hero(
                        tag: filament.name,
                        child: Image.network(filament.photoUrl),
                      ),
                    ),
                  ),
                ),
              ),
            ),
          );
        },
      ),
    );

    // chips.add(Chip(
    //     avatar: CircleAvatar(backgroundImage: NetworkImage(filament.photoUrl),),
    //     label: Text(filament.name),
    //   )
    // );
  });

  return Wrap(
    alignment: WrapAlignment.center,
    spacing: 8.0, // gap between adjacent chips
    runSpacing: 4.0, // gap between lines
    children: gds,
  );
  // return Container(
  //   //aspectRatio: 16/15,
  //   height: 180.0,
  //   child: ListView.builder(
  //     physics: BouncingScrollPhysics(),
  //     itemCount: filaments.length,
  //     scrollDirection: Axis.horizontal,
  //     itemBuilder: (context, i) => Padding(
  //           padding: const EdgeInsets.only(bottom: 10.0, right: 0.0),
  //           child: Column(
  //             children: <Widget>[
  //               InkResponse(
  //                 child: SizedBox(
  //                   child: Hero(
  //                     tag: filaments[i].name,
  //                     child: Container(
  //                           height: 130.0,
  //                           width: 130.0,
  //                           child: Material(
  //                             elevation: 0.0,
  //                             color: Colors.transparent,
  //                             shape: CircleBorder(),
  //                             child: ClipRRect(borderRadius: BorderRadius.circular(65.0),child: Image.network(filaments[i].photoUrl),),
  //                           ),
  //                         )
  //                   ),
  //                 ),
  //                 onTap: () {
  //                    Navigator.of(context).push(
  //                         MaterialPageRoute(
  //                           builder: (ctx) => Scaffold(
  //                             appBar: AppBar(
  //                               centerTitle: true,
  //                               title: Text(filaments[i].name)
  //                             ),
  //                             body: Container(
  //                               color: Colors.black,
  //                               child: GestureDetector(
  //                                 onTap: (){
  //                                   Navigator.pop(context);
  //                                 },
  //                                 child: Center(
  //                                   child:Hero(
  //                                     tag: filaments[i].name,
  //                                     child: Image.network(
  //                                       filaments[i].photoUrl
  //                                     ),
  //                                   )
  //                                 ),
  //                               )
  //                             )
  //                           )
  //                         )
  //                       );
  //                 },
  //               ),
  //               SizedBox(
  //                 width: 150.0,
  //                 child: Padding(
  //                   // padding: EdgeInsets.fromLTRB(16.0, 12.0, 16.0, 8.0),
  //                   padding: EdgeInsets.fromLTRB(0.0, 15.0, 0.0, 0.0),
  //                   child: Column(
  //                     crossAxisAlignment: CrossAxisAlignment.start,
  //                     children: <Widget>[
  //                       Center(
  //                         child: Text(
  //                           filaments[i].name.toUpperCase(),
  //                           style: TextStyle(
  //                               fontSize: 14.0,
  //                               fontWeight: FontWeight.w500,
  //                               color: Colors.white),
  //                           maxLines: 1,
  //                           overflow: TextOverflow.ellipsis,
  //                         ),
  //                       ),
  //                       SizedBox(height: 0.0),
  //                     ],
  //                   ),
  //                 ),
  //               ),
  //             ],
  //           ),
  //         ),
  //   ),
  // );
}

Widget usersRow(List<User> users) {
  return Container(
    //aspectRatio: 16/15,
    height: 200.0,
    child: ListView.builder(
      physics: BouncingScrollPhysics(),
      itemCount: users.length,
      scrollDirection: Axis.horizontal,
      itemBuilder: (context, i) => Padding(
        padding: const EdgeInsets.only(bottom: 10.0, right: 0.0),
        child: Column(
          children: <Widget>[
            InkResponse(
              child: SizedBox(
                child: Hero(
                  tag: users[i].name,
                  child: Container(
                      height: 130.0,
                      width: 130.0,
                      child: CircleAvatar(
                        backgroundImage: NetworkImage(users[i].photoUrl),
                      )

                      // child: Material(
                      //   elevation: 0.0,
                      //   color: Colors.transparent,
                      //   shape: CircleBorder(),
                      //   child: ClipRRect(
                      //       borderRadius: BorderRadius.circular(30.0),
                      //       child: Image.asset(users[i].path),),
                      // ),

                      ),
                ),
              ),
              onTap: () {},
            ),
            SizedBox(
              width: 150.0,
              child: Padding(
                // padding: EdgeInsets.fromLTRB(16.0, 12.0, 16.0, 8.0),
                padding: EdgeInsets.fromLTRB(0.0, 15.0, 0.0, 0.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Center(
                      child: Text(
                        '${users[i].name.toUpperCase()} - ${users[i].role.toUpperCase()}',
                        textAlign: TextAlign.center,
                        style: TextStyle(
                            fontSize: 12.0,
                            fontWeight: FontWeight.w500,
                            color: Colors.black),
                        maxLines: 2,
                        overflow: TextOverflow.ellipsis,
                      ),
                    ),
                    SizedBox(height: 0.0),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    ),
  );
}

Widget igRow(List<InstagramPost> igPosts) {
  return igPosts.isEmpty
      ? Center(
          child: Padding(
            padding: EdgeInsets.all(20),
            child: Text(
              'Could not fetch posts at this time.',
              style: TextStyle(fontWeight: FontWeight.bold),
            ),
          ),
        )
      : Container(
          height: 225.0,
          child: ListView.builder(
            physics: BouncingScrollPhysics(),
            itemCount: igPosts.length,
            scrollDirection: Axis.horizontal,
            itemBuilder: (context, i) {
              return Padding(
                padding: const EdgeInsets.only(bottom: 35.0),
                child: InkWell(
                  onTap: () {
                    Navigator.of(context).push(
                      MaterialPageRoute(
                        builder: (ctx) => PostDetailsPage(
                            igPosts[i].photoUrl,
                            '${igPosts[i].likes} likes',
                            igPosts[i].id,
                            htmlUnescape.convert(igPosts[i].caption),
                            igPosts[i].link),
                      ),
                    );
                  },
                  child: Card(
                    color: Colors.teal,
                    elevation: 0.0,
                    child: ClipRRect(
                      borderRadius: BorderRadius.circular(0.0),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          SizedBox(
                            child: Hero(
                              tag: igPosts[i].id,
                              child: Container(
                                height: 120.0,
                                width: 180.0,
                                decoration: BoxDecoration(
                                  image: DecorationImage(
                                    image: NetworkImage(igPosts[i].photoUrl),
                                    fit: BoxFit.cover,
                                  ),
                                ),
                              ),
                            ),
                          ),
                          SizedBox(
                            width: 180.0,
                            child: Padding(
                              // padding: EdgeInsets.fromLTRB(16.0, 12.0, 16.0, 8.0),
                              padding: EdgeInsets.fromLTRB(10.0, 8.0, 0.0, 0.0),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  Text(
                                    igPosts[i].caption.substring(0, 20) + '...',
                                    style: TextStyle(
                                        fontSize: 15.0,
                                        fontWeight: FontWeight.w500,
                                        color: Colors.white),
                                    maxLines: 1,
                                  ),
                                  SizedBox(height: 5.0),
                                  Padding(
                                    padding: const EdgeInsets.only(bottom: 5.0),
                                    child: Text(
                                      'Posted ${timeago.format(igPosts[i].created_time)}',
                                      style: TextStyle(
                                          fontSize: 12.0,
                                          fontWeight: FontWeight.w500,
                                          color: Colors.white),
                                      maxLines: 1,
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          )
                        ],
                      ),
                    ),
                  ),
                ),
              );
            },
          ),
        );
}

Widget bloggerRow(List<BloggerPost> bloggerPosts) {
  return bloggerPosts.isEmpty
      ? Center(
          child: Padding(
            padding: EdgeInsets.all(20),
            child: Text(
              'Could not fetch blog posts at this time.',
              style: TextStyle(fontWeight: FontWeight.bold),
            ),
          ),
        )
      : Container(
          height: 225.0,
          child: ListView.builder(
            physics: BouncingScrollPhysics(),
            itemCount: bloggerPosts.length,
            scrollDirection: Axis.horizontal,
            itemBuilder: (context, i) {
              return Padding(
                padding: const EdgeInsets.only(bottom: 35.0),
                child: InkWell(
                  onTap: () {
                    URLLauncher.launchUrl(bloggerPosts[i].url);
                  },
                  child: Card(
                    color: Colors.green,
                    elevation: 0.0,
                    child: ClipRRect(
                      borderRadius: BorderRadius.circular(0.0),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          SizedBox(
                            child: Hero(
                              tag: bloggerPosts[i].id,
                              child: Container(
                                height: 120.0,
                                // width: 180.0,
                                width: 220.0,

                                decoration: BoxDecoration(
                                  image: DecorationImage(
                                    image:
                                        NetworkImage(bloggerPosts[i].imageUrl),
                                    fit: BoxFit.cover,
                                  ),
                                ),
                              ),
                            ),
                          ),
                          SizedBox(
                            width: 180.0,
                            child: Padding(
                              // padding: EdgeInsets.fromLTRB(16.0, 12.0, 16.0, 8.0),
                              padding: EdgeInsets.fromLTRB(10.0, 8.0, 0.0, 0.0),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  Text(
                                    bloggerPosts[i].title.length > 30
                                        ? bloggerPosts[i]
                                                .title
                                                .substring(0, 29) +
                                            '...'
                                        : bloggerPosts[i].title,
                                    style: TextStyle(
                                        fontSize: 15.0,
                                        fontWeight: FontWeight.w500,
                                        color: Colors.white),
                                    maxLines: 1,
                                  ),
                                  SizedBox(height: 5.0),
                                  Padding(
                                    padding: const EdgeInsets.only(bottom: 5.0),
                                    child: Text(
                                      'Posted ${timeago.format(bloggerPosts[i].published)}',
                                      style: TextStyle(
                                          fontSize: 12.0,
                                          fontWeight: FontWeight.w500,
                                          color: Colors.white),
                                      maxLines: 1,
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          )
                        ],
                      ),
                    ),
                  ),
                ),
              );
            },
          ),
        );
}

Widget ytRow(List<YouTubeVideo> ytVideos) {
  return ytVideos.isEmpty
      ? Center(
          child: Padding(
            padding: EdgeInsets.all(20),
            child: Text(
              'Could not fetch videos at this time.',
              style: TextStyle(fontWeight: FontWeight.bold),
            ),
          ),
        )
      : Container(
          height: 225.0,
          child: ListView.builder(
            physics: BouncingScrollPhysics(),
            itemCount: ytVideos.length,
            scrollDirection: Axis.horizontal,
            itemBuilder: (context, i) {
              return Padding(
                padding: const EdgeInsets.only(bottom: 35.0),
                child: InkWell(
                  onTap: () {
                    print('Video ID - ${ytVideos[i].id}');
                    Navigator.of(context).push(
                      MaterialPageRoute(
                        builder: (ctx) => Scaffold(
                          appBar: AppBar(
                              centerTitle: true,
                              title: Text(
                                htmlUnescape.convert(ytVideos[i].title),
                              ),
                              actions: <Widget>[]),
                          body: Container(
                            color: Colors.white,
                            child: Center(
                              child: Hero(
                                tag: ytVideos[i].id,
                                child: Material(
                                  child: Center(
                                    // child: FlutterYoutubeView(
                                    //   // onViewCreated: _onYoutubeCreated,
                                    //   // listener: this,
                                    //   params: YoutubeParam(
                                    //       videoId: ytVideos[i].id,
                                    //       showUI: true,
                                    //       startSeconds: 0.0),
                                    // ),
                                  ),
                                ),
                              ),
                            ),
                          ),
                        ),
                      ),
                    );
                  },
                  child: Card(
                    color: Colors.deepPurple,
                    elevation: 0.0,
                    child: ClipRRect(
                      borderRadius: BorderRadius.circular(0.0),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          SizedBox(
                            child: Hero(
                              tag: ytVideos[i].id,
                              child: Container(
                                height: 120.0,
                                width: 180.0,
                                decoration: BoxDecoration(
                                  image: DecorationImage(
                                    image:
                                        NetworkImage(ytVideos[i].thumbnailUrl),
                                    fit: BoxFit.cover,
                                  ),
                                ),
                              ),
                            ),
                          ),
                          SizedBox(
                            width: 180.0,
                            child: Padding(
                              // padding: EdgeInsets.fromLTRB(16.0, 12.0, 16.0, 8.0),
                              padding: EdgeInsets.fromLTRB(10.0, 8.0, 0.0, 0.0),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  Text(
                                    ytVideos[i].title.length >= 20
                                        ? ytVideos[i].title.substring(0, 20) +
                                            '...'
                                        : ytVideos[i].title,
                                    style: TextStyle(
                                        fontSize: 15.0,
                                        fontWeight: FontWeight.w500,
                                        color: Colors.white),
                                    maxLines: 1,
                                  ),
                                  SizedBox(height: 5.0),
                                  Padding(
                                    padding: const EdgeInsets.only(bottom: 5.0),
                                    child: Text(
                                      'Posted ${timeago.format(ytVideos[i].publishedAt)}',
                                      style: TextStyle(
                                          fontSize: 12.0,
                                          fontWeight: FontWeight.w500,
                                          color: Colors.white),
                                      maxLines: 1,
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          )
                        ],
                      ),
                    ),
                  ),
                ),
              );
            },
          ),
        );
}

// Widget filamentRow(){
//   return Container(
//     height: 215.0,
//     child:ListView.builder(
//       itemCount: filaments.length,
//       scrollDirection: Axis.horizontal,
//       itemBuilder: (context, i) => Padding(
//             padding: const EdgeInsets.only(bottom: 35.0),
//             child: InkWell(
//               onTap: () {
//                 Navigator.of(context).push(
//                             MaterialPageRoute(
//                               builder: (ctx) => Scaffold(
//                                 appBar: AppBar(
//                                   centerTitle: true,
//                                   title: Text(filaments[i].name)
//                                 ),
//                                 body: Container(
//                                   color: Colors.black,
//                                   child: GestureDetector(
//                                     onTap: (){
//                                       Navigator.pop(context);
//                                     },
//                                     child: Center(
//                                       child:Hero(
//                                         tag: filaments[i].name,
//                                         child: Image.network(
//                                           filaments[i].photoUrl
//                                         ),
//                                       )
//                                     ),
//                                   )
//                                 )
//                               )
//                             )
//                           );
//               },
//               child:Card(
//                 elevation: 0.0: 12.0,
//                 child: ClipRRect(
//                   borderRadius: BorderRadius.circular(6.0),
//                   child: Column(
//                     crossAxisAlignment: CrossAxisAlignment.start,
//                     children: <Widget>[
//                       SizedBox(
//                         child: Hero(
//                           tag: filaments[i].name,
//                           child: Container(
//                                   height: 120.0,
//                                   width: 180.0,
//                                   decoration: BoxDecoration(
//                                       image: DecorationImage(
//                                     image: NetworkImage(filaments[i].photoUrl),
//                                     fit: BoxFit.cover,
//                                   ),),
//                                 )
//                         ),
//                       ),
//                       SizedBox(
//                           width: 180.0,
//                           child: Padding(
//                             // padding: EdgeInsets.fromLTRB(16.0, 12.0, 16.0, 8.0),
//                             padding: EdgeInsets.fromLTRB(10.0, 8.0, 0.0, 0.0),
//                             child: Column(
//                               crossAxisAlignment: CrossAxisAlignment.start,
//                               children: <Widget>[
//                                 Text(
//                                   filaments[i].name,
//                                   style:TextStyle(
//                                       fontSize: 15.0,
//                                       fontWeight: FontWeight.w500,
//                                       color: Colors.white
//                                   ),
//                                   maxLines: 1,
//                                 ),
//                                 SizedBox(height: 5.0),
//                                 Padding(
//                                   padding: const EdgeInsets.only(bottom: 5.0),
//                                   child: Text(
//                                     '1.75mm PLA',
//                                     style: TextStyle(
//                                         fontSize: 10.0,
//                                                                                 fontWeight: FontWeight.w500,
//                                         color:
//                                             Colors.white),
//                                     maxLines: 1,
//                                   ),
//                                 ),
//                               ],
//                             ),
//                           ),)
//                     ],
//                   ),
//                 ),
//               ),
//             ),
//           ),
//     ),
//   );
// }

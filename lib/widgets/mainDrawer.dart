// import 'package:flutter/material.dart';
// import 'package:tr3designs_flutter/pages/suggestions.dart';
// import 'package:tr3designs_flutter/pages/home.dart';
// import 'package:tr3designs_flutter/pages/shop.dart';
// import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
// import 'package:tr3designs_flutter/pages/about.dart';
// import 'package:tr3designs_flutter/pages/settings.dart';
// import 'package:tr3designs_flutter/pages/filaments.dart';
// import 'package:tr3designs_flutter/constants.dart';

// class MainDrawer extends StatefulWidget {
//   MainDrawer(this._page);
//   final String _page;

//   @override
//   State createState() => MainDrawerState(_page);
// }

// class MainDrawerState extends State<MainDrawer> {
//   MainDrawerState(this._page);

//   final String _page;
//   final Color _iconColor = Colors.orange;

//   @override
//   void initState() {
//     super.initState();
//   }

//   @override
//   Widget build(BuildContext context) {
//     return Drawer(
//         child: ListView(
//       children: <Widget>[
//         new UserAccountsDrawerHeader(
//           accountName: Text('Santos Enoque'),
//           accountEmail: Text('santosenoque.ss@gmail.com'),
//           currentAccountPicture: GestureDetector(
//             child: new CircleAvatar(
//               backgroundColor: Colors.grey,
//               child: Icon(
//                 Icons.person,
//                 color: Colors.white,
//               ),
//             ),
//           ),
//           decoration: new BoxDecoration(color: Colors.orange),
//         ),
//         ListTile(
//           leading: Icon(Icons.home, color: this._iconColor),
//           title: Text('Home'),
//           onTap: () {
//             if (this._page != 'Home') {
//               Navigator.push(
//                 context,
//                 MaterialPageRoute(builder: (context) => HomePage()),
//               );
//             } else {
//               Navigator.of(context).pop();
//             }
//           },
//         ),
//         //Suggestions
//         ListTile(
//           leading: Icon(Icons.lightbulb_outline, color: this._iconColor),
//           title: Text('Suggestions'),
//           subtitle: Text('Create and vote on ideas.'),
//           onTap: () {
//             if (this._page != 'Suggestions') {
//               Navigator.push(
//                 context,
//                 MaterialPageRoute(builder: (context) => SuggestionsPage()),
//               );
//             } else {
//               Navigator.of(context).pop();
//             }
//           },
//         ),
//         //Filaments
//         ListTile(
//           leading: Icon(Icons.donut_large, color: this._iconColor),
//           title: Text('Filaments'),
//           subtitle: Text('See our list of color options.'),
//           onTap: () {
//             if (this._page != 'Filaments') {
//               Navigator.push(
//                 context,
//                 MaterialPageRoute(builder: (context) => FilamentsPage()),
//               );
//             } else {
//               Navigator.of(context).pop();
//             }
//           },
//         ),
//         //Shop
//         ListTile(
//           leading: Icon(Icons.store, color: this._iconColor),
//           title: Text('Shop'),
//           subtitle: Text(TAG_SHOP),
//           onTap: () {
//             if (this._page != 'Shop') {
//               Navigator.push(
//                 context,
//                 MaterialPageRoute(builder: (context) => ShopPage()),
//               );
//             } else {
//               Navigator.of(context).pop();
//             }
//           },
//         ),
//         Divider(),
//         //Settings
//         ListTile(
//           leading: Icon(Icons.settings, color: this._iconColor),
//           title: Text('Settings'),
//           onTap: () {
//             if (this._page != 'Settings') {
//               Navigator.push(
//                 context,
//                 MaterialPageRoute(builder: (context) => SettingsPage()),
//               );
//             } else {
//               Navigator.of(context).pop();
//             }
//           },
//         ),
//         //About
//         ListTile(
//           leading: Icon(MdiIcons.help, color: this._iconColor),
//           title: Text('About'),
//           onTap: () {
//             if (this._page != 'About') {
//               Navigator.push(
//                 context,
//                 MaterialPageRoute(builder: (context) => AboutPage()),
//               );
//             } else {
//               Navigator.of(context).pop();
//             }
//           },
//         ),
//       ],
//     ));
//   }
// }

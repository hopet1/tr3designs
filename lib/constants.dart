import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:flutter/material.dart';
import 'package:intro_views_flutter/Models/page_view_model.dart';

// const String ADMIN_FACEBOOK_ID = '10215920257112057'; //Trey Hope Facebook ID.
const String ADMIN_DEVICE_ID = '284b79f5b695ced1'; //Trey Hope, Samsung Galaxy S9
const String GOOGLE_FCM_SERVER_KEY =
    'AAAAGnGMhCk:APA91bHbPJDxqW68Jpg9AAoIDAHqjJNeNYbxRgKP-aFGxVC0_m3Mzt9St0-LciU-X7BsvtKUDTguKzfpPAD75AWwWg_1jFZn6MPZwrUAWKBvanDuaCxX9pa95WRTGF8kM5DiyU5wOTjJ';
const String GOOGLE_API_KEY =
    'AIzaSyBqF0AgNZuyjSnq0NKMZKdPolNoKfCrJsQ'; //API for all Google APIs
const String BLOGGER_BLOG_ID =
    '5736572161410833289'; //tr3Designs Blogger blog ID.
const String ETSY_FIELDS = 'url,price,title,shop_section_id,description,views,num_favorers,is_customizable,listing_id';
const String YOUTUBE_CHANNEL_ID =
    'UCyMTUp7B-lFoRbDfLr3QuJw'; //tr3Designs YouTube channel ID.
const String IG_API_ACCESS_TOKEN =
    '7219705419.77441c8.c2c658a6ef5248cdab7ae0d9ea0c76c0'; //tr3Designs Instagram Access Token.
const String FACEBOOK_ACCESS_TOKEN =
    'EAACj2beZB55wBAOZBrieV3qATYZC8tz0Mx0BAMCejxkp7QPTlNyGAYT2ksxcOXckKyIx454EzhhAblSJxUNEbBUhP5kDnNKSZCJsvUWWOhjeI6VXGapF4rfYi2Bdkz0NPU6z2ez4wOkJA105r49PNDTfMiH7PZB2esMb86ODGTLP9of9DHy6K7ZACIvCk1kCex3Cx6k4wSCl2isJJRkmYW2Ju1dE86MPAZD';

const String TOPIC_NEW_PRODUCT = 'NEW_PRODUCT';
const String TOPIC_WEEKLY_GIVEAWAY = 'WEEKLY_GIVEAWAY';

const String MAILING_LIST_URL = 'http://eepurl.com/dNMyGQ';
const String GOOGLE_FCM_ENDPOINT = 'https://fcm.googleapis.com/fcm/send';

const double EXPANDED_APPBAR_HEIGHT = 175;
const double HOME_ICON_SIZE = 75;

class Filament {
  String photoUrl;
  String name;
  String url;

  Filament(this.photoUrl, this.name, this.url);
}

class SocialM {
  String url;
  Icon icon;

  SocialM(this.url, this.icon);
}

final INTRO_PAGES = [
  PageViewModel(
      pageColor: const Color(0xFF03A9F4),
      // iconImageAssetPath: 'assets/air-hostess.png',
      // bubble: Image.asset('assets/air-hostess.png'),
      body: Text(
        'Stay up to date with videos, blogs, and much more.',
      ),
      title: Text(''),
      textStyle: TextStyle(fontFamily: 'MyFont', color: Colors.white),
      mainImage: Image.asset(
        'assets/images/social_media.png',
        height: 285.0,
        width: 285.0,
        alignment: Alignment.center,
      )),
  PageViewModel(
    pageColor: const Color(0xFF8BC34A),
    body: Text(
      'Let us know what ideas you have for tr3Designs.',
    ),
    title: Text(''),
    mainImage: Image.asset(
      'assets/images/think.png',
      height: 285.0,
      width: 285.0,
      alignment: Alignment.center,
    ),
    textStyle: TextStyle(fontFamily: 'MyFont', color: Colors.white),
  ),
  PageViewModel(
    pageColor: const Color(0xFF607D8B),
    body: Text(
      'View/shop from our list of products.',
    ),
    title: Text(''),
    mainImage: Image.asset(
      'assets/images/shop.png',
      height: 285.0,
      width: 285.0,
      alignment: Alignment.center,
    ),
    textStyle: TextStyle(fontFamily: 'MyFont', color: Colors.white),
  ),
];

final List<SocialM> socialMedias = [
  //Twitter
  SocialM('https://twitter.com/tr3designs',
      Icon(MdiIcons.twitter, size: HOME_ICON_SIZE, color: Colors.blue)),
  //Instagram
  SocialM('https://www.instagram.com/tr3.designs',
      Icon(MdiIcons.instagram, size: HOME_ICON_SIZE, color: Colors.purple)),
  //YouTube
  SocialM('https://www.youtube.com/channel/UCyMTUp7B-lFoRbDfLr3QuJw',
      Icon(MdiIcons.youtube, size: HOME_ICON_SIZE, color: Colors.red)),
  //Facebook
  SocialM('https://www.facebook.com/tr3Designs/',
      Icon(MdiIcons.facebook, size: HOME_ICON_SIZE, color: Colors.blue)),
  //Pinterest
  SocialM('https://www.pinterest.com/tr3Designs/',
      Icon(MdiIcons.pinterest, size: HOME_ICON_SIZE, color: Colors.red)),
  //Etsy
  SocialM('https://www.etsy.com/shop/trxDxsxgns',
      Icon(MdiIcons.etsy, size: HOME_ICON_SIZE, color: Colors.orange)),
];

final List<Filament> filaments = [
  //Black
  Filament(
      'https://lh3.googleusercontent.com/FzaMBj_rQz3Q6iF-HkU3exVnmuCK8z2ZMKUElUwF5lPGLaIP0DBIDvRYPvQZN-rx-A-a3X7sSgRDhLi7BwjplSs=w640-h480-p',
      'Black',
      'https://www.matterhackers.com/store/l/175mm-pla-filament-black-1-kg/sk/MY6CYEZM'),
  //Blue
  Filament(
      'https://lh3.googleusercontent.com/jlHMx_917PtcOfS0jvFNx4xFulj8oCQeSXIZ4JK9C5s8wMU5PaSAs26DfyZYyBvU73Na3y7_sckdjAtx64_m1dg=w640-h480-p',
      'Blue',
      'https://www.matterhackers.com/store/l/175mm-pla-filament-blue-1-kg/sk/MMQFQZ5S'),
  //Light Blue
  Filament(
      'https://lh3.googleusercontent.com/O-V4nsgCIVqw0-UNlR08tzkTDOR87lu2ci7vDQKsiO0amzXpiOeYkEPHUHgmHwgEu_2v01jlznqxPBnGhbb6_Zmf=w640-h480-p',
      'Light Blue',
      'https://www.matterhackers.com/store/l/300mm-pla-filament-lightblue-1-kg/sk/MNZJMHJ3'),
  //Brown
  Filament(
      'https://lh3.googleusercontent.com/b2hSCvcFsddUmoJ8kyhH4loGKnsjq3GjGt4rK5J7R0yWoD2XU96sa6JffyNXGTNMCfOF3MHwVBPNr7DJbPjgD3S5=w640-h480-p',
      'Brown',
      'https://www.matterhackers.com/store/l/175mm-pla-filament-brown-1-kg/sk/MPS8DESQ'),
  //Glow In the Dark
  Filament(
      'https://lh3.googleusercontent.com/DlBViEL0lJIUU1ifXxo83wPhjFPjCLkfJMhupjfJhGsg0waV_ddlj9nO-Ypz-Yfqpmsh7FoUFisYN7DAWB6bxp9L=w640-h480-p',
      'Glow In the Dark',
      'https://www.matterhackers.com/store/l/blue-glow-in-the-dark-pla-filament-175mm/sk/MPXTSP38'),
  //Gold
  Filament(
      'https://lh3.googleusercontent.com/CjvKIdVMZ5K5YyDOyF3JpvuJe-iqFKEcfdT-dToYYTRN_ApWuRvrDtzEzWvUq-i1EVg-jXl3iIYjqGO06faltiM=w640-h480-p',
      'Gold',
      'https://www.matterhackers.com/store/l/175mm-pla-filament-gold-1-kg/sk/M9UV322N'),
  //Green
  Filament(
      'https://lh3.googleusercontent.com/FtZeZ5dFvk5WVIPYKva7unWtNtzXf-7XaB9Wipspjy3jVC3lPJXR-Y28vSfmdAbr-2J6r_AKnaARCZeZHvPErqa7=w640-h480-p',
      'Green',
      'https://www.matterhackers.com/store/l/forest-green-pla-filament-1.75mm/sk/MUAAMXWX'),
  //Light Green
  Filament(
      'https://lh3.googleusercontent.com/PkLKKMaNnPMzkfTcDP9kveJrr2hgcmExz3DZebZIeFFcMOxwYqO94JKNwoQUw6Q8020P4ssJo-42m85PSANpx5k=w640-h480-p',
      'Light Green',
      'https://www.matterhackers.com/store/l/175mm-pla-filament-limegreen-1-kg/sk/MXMHA4WM'),
  //Grey
  Filament(
      'https://lh3.googleusercontent.com/ae8PSVJknKCxNWp1iu4qe_LI1bM_Jo1S6Q1MBGWkmZbmcAOMsScBqmJYFkncxmVp4LM_teuzD91UBFwtW3venzE=w640-h480-p',
      'Grey',
      'https://www.matterhackers.com/store/l/175mm-pla-filament-grey-1-kg/sk/MPL0Q8ZD'),
  //Pink
  Filament(
      'https://lh3.googleusercontent.com/-sFJeC7ioqAD8Jcn9goL1GupUqgrWOZfImGpO3Vp5UJHUFajqIz7TARsjbXtLTZLtUQUij9-ATWvLj8t2L5bVk4=w640-h480-p',
      'Pink',
      'https://www.matterhackers.com/store/l/175mm-pla-filament-pink-1-kg/sk/M81ZR601'),
  //Purple
  Filament(
      'https://lh3.googleusercontent.com/wFkQ4WCDvEiDMjcZD9EDKYBCO719Tva3IC3gmJJWxHoU01lsRlACAJuJZJz-uUrwoY4kqCVMxA2rDptfnrouYrU=w640-h480-p',
      'Purple',
      'https://www.matterhackers.com/store/l/175mm-pla-filament-purple-1-kg/sk/M46G60RF'),
  //Magenta
  Filament(
      'https://lh3.googleusercontent.com/ov2jyVR6FT4VDPA3ZQdnmnH5wpR4fM41i8Fn-jNE_jCjAh0gk3r9gI__EqMW9QiH4Gcr7wXn67qJaW09yoUAVMA=w640-h480-p',
      'Magenta',
      'https://www.matterhackers.com/store/l/magenta-pla-filament-1.75mm/sk/M9G7ATE3'),
  //Natural
  Filament(
      'https://lh3.googleusercontent.com/1dJc0rgbhzBXzVa-dcYQtftwi5VUbTDqVVVZsxUkSGqdppE8DDRSBHdkD9Oq3wb6c7euYQP8CA9m-qK6SARuWwk=w640-h480-p',
      'Natural',
      'https://www.matterhackers.com/store/l/175mm-pla-filament-natural-1-kg/sk/M0QV78YA'),
  //Orange
  Filament(
      'https://lh3.googleusercontent.com/zdjG3kBK_EbrBDn1kSoob1T88N6R-LgRCqw2XlnDDS333BQO2WxJ3Byj2n5UHVaiULP2p7v8BSV4leSC00BqYg=w640-h480-p',
      'Orange',
      'https://www.matterhackers.com/store/l/175mm-pla-filament-orange-1-kg/sk/ME4CUFC6'),
  //Red
  Filament(
      'https://lh3.googleusercontent.com/Q2y1KQ0v-_jLizcfenry34c1RCH2iT8gcZw-pnUIjSVODk4whj1fMkyjC5Omhx7sMsF6fQB7RtFzVYK1C1AfqZE=w640-h480-p',
      'Red',
      'https://www.matterhackers.com/store/l/175mm-pla-filament-red-1-kg/sk/MPV1RLHJ'),
  //White
  Filament(
      'https://lh3.googleusercontent.com/RsJKmFzIAPtOgZVSwTGXNlWZAlaJHxxXQyE6Jwuv1M_uSxrDBUSA8zgZDFGmt6J1HoV45u3S9G84tosPcU4q13py=w640-h480-p',
      'White',
      'https://www.matterhackers.com/store/l/175mm-pla-filament-white-1-kg/sk/MEEDKTKU'),
  //Yellow
  Filament(
      'https://lh3.googleusercontent.com/z4I2r48OI6axjLzkjXLf6saiPihmuZ51tGK7Af56Dd5d6pD2l7pRVfNdhoUsZszbtIsVNFYDPo-TFhE5vvGdS4gr=w640-h480-p',
      'Yellow',
      'https://www.matterhackers.com/store/l/175mm-pla-filament-yellow-1-kg/sk/M4L474LS'),
];

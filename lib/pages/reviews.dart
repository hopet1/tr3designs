import 'package:flutter/material.dart';
import 'package:tr3designs_flutter/models/review.dart';
import 'package:tr3designs_flutter/services/thirdPartyAPI.dart';
import 'dart:convert' show json;
import 'package:timeago/timeago.dart' as timeago;
import 'package:tr3designs_flutter/services/urlLauncher.dart';

class ReviewsPage extends StatefulWidget {
  @override
  State createState() => ReviewsPageState();
}

class ReviewsPageState extends State<ReviewsPage>
    with SingleTickerProviderStateMixin {
  List<Review> reviews = List<Review>();
  bool _isLoading = true;

  @override
  void initState() {
    super.initState();
    loadPage();
  }

  Future getReviews() async {
    String feedbackData = await EtsyAPI.getFeedback();
    final Map feedbackParsed = json.decode(feedbackData);

    for (var result in feedbackParsed['results']) {
      if (result['message'] != null) {
        Review review = Review();
        review.message = result['message'];
        review.creation_tsz =
            DateTime.fromMillisecondsSinceEpoch(result['creation_tsz'] * 1000);
        reviews.add(review);
      }
    }
  }

  void loadPage() async {
    await getReviews();

    setState(() {
      this._isLoading = false;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text('Reviews'),
      ),
      body: _isLoading
          ? Center(
              child: CircularProgressIndicator(),
            )
          : ListView.builder(
              itemBuilder: (BuildContext ctxt, int index) {
                return _buildListItem(reviews[index]);
              },
              itemCount: reviews.length,
            ),
      bottomNavigationBar: _buildBottomNavigationBar(),
    );
  }

  Widget _buildListItem(Review review) {
    return Column(
      children: <Widget>[
        ListTile(
          leading: Icon(Icons.thumb_up, color: Colors.green),
          subtitle: Text('Posted ${timeago.format(review.creation_tsz)}'),
          title: Text(review.message),
        ),
        Divider()
      ],
    );
  }

  _buildBottomNavigationBar() {
    return Container(
      width: MediaQuery.of(context).size.width,
      height: 50.0,
      child: RaisedButton(
        onPressed: () {
          URLLauncher.launchUrl('https://www.etsy.com/shop/trxDxsxgns');
        },
        color: Colors.blue,
        child: Center(
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Icon(
                Icons.rate_review,
                color: Colors.white,
              ),
              SizedBox(
                width: 4.0,
              ),
              Text(
                'LEAVE A REVIEW',
                style: TextStyle(color: Colors.white),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

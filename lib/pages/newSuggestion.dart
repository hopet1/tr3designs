import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:tr3designs_flutter/services/modal.dart';
import 'package:tr3designs_flutter/services/validation.dart';
import 'package:tr3designs_flutter/widgets/inputField.dart';
// import 'package:device_id/device_id.dart';

class NewSuggestionPage extends StatefulWidget {
  @override
  State createState() => NewSuggestionPageState();
}

class NewSuggestionPageState extends State<NewSuggestionPage>
    with SingleTickerProviderStateMixin {
  final _db = Firestore.instance;
  final TextEditingController _descriptionController = TextEditingController();
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  final int _maxLength = 30;
  bool _autovalidate = false;
  // String _deviceId;
  bool _isLoading = true;

  @override
  void initState() {
    super.initState();

    loadPage();
  }

  loadPage() async {
    //Get device id.
    // _deviceId = await DeviceId.getID;

    setState(() {
      this._isLoading = false;
    });
  }

  submit() {
    final FormState form = this._formKey.currentState;

    if (!form.validate()) {
      this._autovalidate = true; // Start validating on every change.
      Modal.showInSnackBar(context, 'Please fix the errors before submitting.');
    } else {
      showDialog(
        barrierDismissible: false,
        builder: (_) => AlertDialog(
          title: Text('Not a bad idea. Send now?'),
          content: Text(_descriptionController.text),
          actions: <Widget>[
            FlatButton(
              onPressed: () {
                Navigator.pop(context);
              },
              child: Text('Cancel'),
            ),
            FlatButton(
              onPressed: () async {
                Navigator.pop(context);

                var dataMap = Map<String, dynamic>();

                dataMap['description'] = _descriptionController.text;
                dataMap['likes'] = 0;
                dataMap['users'] = []; // People who like idea.
                // dataMap['user'] = _deviceId; // Person who posted idea.
                dataMap['safe'] = false;
                dataMap['time'] = DateTime.now();

                final DocumentReference ds =
                    await _db.collection('Suggestions').add(dataMap);
                await _db
                    .collection('Suggestions')
                    .document(ds.documentID)
                    .updateData({'id': ds.documentID}).then((res) {
                  _descriptionController.clear();
                  Modal.showAlert(context, 'Sent',
                      'Your suggestion is now being reviewed.');
                }).catchError((e) {
                  Modal.showAlert(
                    context,
                    'Error',
                    e.toString(),
                  );
                });
              },
              child: Text('Submit'),
            ),
          ],
        ),
        context: context,
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text('Suggestion'),
      ),
      body: _isLoading
          ? Center(
              child: CircularProgressIndicator(),
            )
          : Builder(
              builder: (context) => Form(
                  autovalidate: this._autovalidate,
                  child: ListView(
                    padding: EdgeInsets.all(20.0),
                    children: <Widget>[
                      Text(
                        'What would you like tr3Designs to make?',
                        textAlign: TextAlign.center,
                        style: TextStyle(fontSize: 24),
                      ),
                      Padding(
                        padding: EdgeInsets.symmetric(vertical: 20.0),
                      ),
                      InputField(
                          // initialValue: '',
                          controller: _descriptionController,
                          labelText: 'Your Suggestion...',
                          hintText:
                              'Tell us in ${_maxLength} characters or less.',
                          obscureText: false,
                          textInputType: TextInputType.text,
                          icon: Icons.lightbulb_outline,
                          iconColor: Colors.black,
                          bottomMargin: 20.0,
                          maxLength: _maxLength,
                          validateFunction: Validation.validateBasic),
                    ],
                  ),
                  key: _formKey),
            ),
      bottomNavigationBar: _buildBottomNavigationBar(),
    );
  }

  _buildBottomNavigationBar() {
    return Container(
      width: MediaQuery.of(context).size.width,
      height: 50.0,
      child: RaisedButton(
        onPressed: () {
          submit();
        },
        color: Colors.blue,
        child: Center(
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Icon(
                Icons.send,
                color: Colors.white,
              ),
              SizedBox(
                width: 4.0,
              ),
              Text(
                'SUBMIT SUGGESTION',
                style: TextStyle(color: Colors.white),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:tr3designs_flutter/services/urlLauncher.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:tr3designs_flutter/models/youTubeVideo.dart';
import 'package:tr3designs_flutter/models/instagramPost.dart';
import 'package:tr3designs_flutter/services/thirdPartyAPI.dart';
import 'dart:convert' show json;
import 'package:tr3designs_flutter/models/bloggerPost.dart';
import 'package:tr3designs_flutter/widgets/dataRows.dart';
import 'package:tr3designs_flutter/pages/suggestions.dart';
import 'package:tr3designs_flutter/pages/shop.dart';
import 'package:tr3designs_flutter/pages/settings.dart';
import 'package:tr3designs_flutter/pages/about.dart';
import 'package:tr3designs_flutter/pages/reviews.dart';
import 'package:tr3designs_flutter/pages/filaments.dart';
// import 'package:cached_network_image/cached_network_image.dart';

class HomePage extends StatefulWidget {
  @override
  State createState() => HomePageState();
}

class HomePageState extends State<HomePage> with TickerProviderStateMixin {
  final _db = Firestore.instance;
  bool _isLoading = true;

  final Color _iconColor = Colors.black;
  List<YouTubeVideo> _ytVideos = List<YouTubeVideo>();
  List<BloggerPost> _blogPosts = List<BloggerPost>();
  List<InstagramPost> _igPosts = List<InstagramPost>();
  List<Image> _images = List<Image>();
  String _announcement;
  int _listing_active_count;

  @override
  void initState() {
    super.initState();

    loadPage();
  }

  loadPage() async {
    await _getImageUrls();
    print('_getImageUrls complete');
    await _getInstagramPosts();
        print('_getInstagramPosts complete');

    await _getBloggerPosts();
    print('_getBloggerPosts complete');


    await _getYouTubeVideos();
    print('_getYouTubeVideos complete');

    // await _getShopInfo();
    // print('_getShopInfo complete');

    setState(() {
      _isLoading = false;
    });
  }

  _getImageUrls() async {
    DocumentSnapshot document =
        await _db.collection('ExtraData').document('HomePage').get();

    for (var imageUrl in document['imageUrls']) {
      _images.add(Image.network(imageUrl));
    }
  }

  _getInstagramPosts() async {
    String igRes = await InstagramAPI.getMedia(5);
    if (igRes != null) {
      final Map igParsed = json.decode(igRes);
      for (var result in igParsed['data']) {
        InstagramPost post = InstagramPost();

        post.id = result['id'];
        post.likes = result['likes']['count'];
        post.photoUrl = result['images']['standard_resolution']['url'];
        post.link = result['link'];
        post.caption = result['caption']['text'];
        post.created_time = DateTime.fromMillisecondsSinceEpoch(
            int.parse(result['created_time']) * 1000);

        _igPosts.add(post);
      }
    }
  }

  _getBloggerPosts() async {
    String blogRes = await BloggerAPI.getAllPosts();
    if (blogRes != null) {
      final Map blogParsed = json.decode(blogRes);
      for (var result in blogParsed['items']) {
        BloggerPost post = BloggerPost();

        post.id = result['id'];
        post.title = result['title'];
        post.url = result['url'];
        post.published = DateTime.parse(result['published']);
        post.content = result['content'];
        post.imageUrl = result['images'][0]['url'];

        _blogPosts.add(post);
      }
    }
  }

  _getYouTubeVideos() async {
    String videoRes = await YouTubeAPI.getVideos(5);
    if (videoRes != null) {
      final Map videoParsed = json.decode(videoRes);
      for (var result in videoParsed['items']) {
        YouTubeVideo video = YouTubeVideo();

        video.id = result['id']['videoId'];
        video.title = result['snippet']['title'];
        video.title = video.title
            .replaceAll('&#39;', '\''); //Remove random characters from title.
        video.publishedAt = DateTime.parse(result['snippet']['publishedAt']);
        video.thumbnailUrl = result['snippet']['thumbnails']['high']['url'];
        video.description = result['snippet']['description'];

        _ytVideos.add(video);
      }
    }
  }

  _getShopInfo() async {
    String shopRes = await EtsyAPI.getShop();
    final Map shopResParsed = json.decode(shopRes);
    _announcement =
        htmlUnescape.convert(shopResParsed['results'][0]['announcement']);
    _listing_active_count = shopResParsed['results'][0]['listing_active_count'];
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text(
          'tr3Designs',
          style: TextStyle(color: Colors.white),
        ),
        actions: <Widget>[
          IconButton(
              icon: Icon(Icons.settings),
              onPressed: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => SettingsPage(),
                  ),
                );
              })
        ],
      ),
      drawer: Drawer(
        child: ListView(
          children: <Widget>[
            UserAccountsDrawerHeader(
              accountName: Text(
                'tr3Designs',
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
              accountEmail: Text('The power of customization.'),
              currentAccountPicture: GestureDetector(
                child: CircleAvatar(
                  backgroundImage: AssetImage('assets/images/logo.png'),
                ),
              ),
              decoration: BoxDecoration(color: Colors.deepPurple),
            ),
            ListTile(
              leading: Icon(Icons.home, color: _iconColor),
              title: Text(
                'Home',
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
              onTap: () {},
            ),
            Divider(),
            //Shop
            // ListTile(
            //   leading: Icon(Icons.store, color: _iconColor),
            //   title: Text(
            //     'Shop',
            //     style: TextStyle(fontWeight: FontWeight.bold),
            //   ),
            //   subtitle: Text('See what products are available.'),
            //   onTap: () {
            //     Navigator.push(
            //       context,
            //       MaterialPageRoute(
            //         builder: (context) => ShopPage(),
            //       ),
            //     );
            //   },
            // ),
            //Suggestions
            ListTile(
              leading: Icon(Icons.lightbulb_outline, color: _iconColor),
              title: Text(
                'Suggestions',
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
              subtitle: Text('Create and vote on ideas.'),
              onTap: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => SuggestionsPage(),
                  ),
                );
              },
            ),
            //Reviews
            // ListTile(
            //   leading: Icon(Icons.rate_review, color: _iconColor),
            //   title: Text(
            //     'Reviews',
            //     style: TextStyle(fontWeight: FontWeight.bold),
            //   ),
            //   subtitle: Text('Don\'t just take our word for it.'),
            //   onTap: () {
            //     Navigator.push(
            //       context,
            //       MaterialPageRoute(
            //         builder: (context) => ReviewsPage(),
            //       ),
            //     );
            //   },
            // ),
            //Filaments
            ListTile(
              leading: Icon(Icons.donut_large, color: _iconColor),
              title: Text(
                'Filaments',
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
              subtitle: Text('See our list of color options.'),
              onTap: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => FilamentsPage(),
                  ),
                );
              },
            ),
            Divider(),
            //About
            // ListTile(
            //   leading: Icon(MdiIcons.help, color: _iconColor),
            //   title: Text(
            //     'About',
            //     style: TextStyle(fontWeight: FontWeight.bold),
            //   ),
            //   onTap: () {
            //     Navigator.push(
            //       context,
            //       MaterialPageRoute(
            //         builder: (context) => AboutPage(),
            //       ),
            //     );
            //   },
            // ),
          ],
        ),
      ),
      body: Builder(
        builder: (context) => _isLoading
            ? Center(
                child: CircularProgressIndicator(),
              )
            : CustomScrollView(physics: BouncingScrollPhysics(), slivers: <
                Widget>[
                SliverList(
                  delegate: SliverChildListDelegate(<Widget>[
                    _buildProductImagesWidgets(),
                    SizedBox(height: 0),
                    // Text(
                    //   '"The power of customization."',
                    //   textAlign: TextAlign.center,
                    //   style:
                    //       TextStyle(fontWeight: FontWeight.bold, fontSize: 15),
                    // ),
                    Container(
                      padding: const EdgeInsets.all(10.0),
                      child: Card(
                        color: Colors.blueGrey,
                        elevation: 4.0,
                        child: Padding(
                          padding: EdgeInsets.all(20),
                          child: Column(
                            // crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Text(
                                "SHOP BETWEEN ${30} DIFFERENT PRODUCTS!",
                                textAlign: TextAlign.center,
                                style: TextStyle(
                                  fontSize: 12,
                                  fontWeight: FontWeight.bold,
                                  color: Colors.white,
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                    Divider(),
                    //Instagram Posts
                    // Padding(
                    //   padding: EdgeInsets.all(15),
                    //   child: Row(
                    //     mainAxisSize: MainAxisSize.max,
                    //     mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    //     children: <Widget>[
                    //       Row(
                    //         children: <Widget>[
                    //           Text(
                    //             'INSTAGRAM POSTS',
                    //             style: TextStyle(
                    //                 fontWeight: FontWeight.bold,
                    //                 fontSize: 12.0,
                    //                 letterSpacing: 2.0,
                    //                 color: Colors.black),
                    //           )
                    //         ],
                    //       ),
                    //       InkWell(
                    //         child: Text(
                    //           'SEE MORE',
                    //           style: TextStyle(
                    //               fontWeight: FontWeight.bold,
                    //               fontSize: 12.0,
                    //               letterSpacing: 2.0,
                    //               color: Colors.blue),
                    //         ),
                    //         onTap: () {
                    //           URLLauncher.launchUrl(
                    //               'https://www.instagram.com/tr3.designs');
                    //         },
                    //       )
                    //     ],
                    //   ),
                    // ),
                    // igRow(_igPosts),
                    //Blog Posts
                    Padding(
                      padding: EdgeInsets.all(15),
                      child: Row(
                        mainAxisSize: MainAxisSize.max,
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Row(
                            children: <Widget>[
                              Text(
                                'BLOG POSTS',
                                style: TextStyle(
                                    fontWeight: FontWeight.bold,
                                    fontSize: 12.0,
                                    letterSpacing: 2.0,
                                    color: Colors.black),
                              )
                            ],
                          ),
                          InkWell(
                            child: Text(
                              'SEE MORE',
                              style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                  fontSize: 12.0,
                                  letterSpacing: 2.0,
                                  color: Colors.blue),
                            ),
                            onTap: () {
                              URLLauncher.launchUrl(
                                  'https://trxdxsxgns.blogspot.com/');
                            },
                          )
                        ],
                      ),
                    ),
                    bloggerRow(_blogPosts),
                    //YouTube Videos
                    Padding(
                      padding: EdgeInsets.all(15),
                      child: Row(
                        mainAxisSize: MainAxisSize.max,
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Row(
                            children: <Widget>[
                              Text(
                                'YOUTUBE VIDEOS',
                                style: TextStyle(
                                    fontWeight: FontWeight.bold,
                                    fontSize: 12.0,
                                    letterSpacing: 2.0,
                                    color: Colors.black),
                              )
                            ],
                          ),
                          InkWell(
                            child: Text(
                              'SEE MORE',
                              style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                  fontSize: 12.0,
                                  letterSpacing: 2.0,
                                  color: Colors.blue),
                            ),
                            onTap: () {
                              URLLauncher.launchUrl(
                                  'https://www.youtube.com/channel/UCyMTUp7B-lFoRbDfLr3QuJw');
                            },
                          )
                        ],
                      ),
                    ),
                    ytRow(_ytVideos),
                    Divider(),
                    Container(
                      padding: const EdgeInsets.all(10.0),
                      child: Card(
                        color: Colors.teal,
                        elevation: 4.0,
                        child: Padding(
                          padding: EdgeInsets.all(20),
                          child: Column(
                            // crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Text(
                                "THINKING OF BUYING BIG?",
                                textAlign: TextAlign.center,
                                style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                  color: Colors.white,
                                ),
                              ),
                              Divider(color: Colors.white),
                              SizedBox(height: 10),
                              Text(
                                'Announcment Coming Soon...',
                                textAlign: TextAlign.center,
                                style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                  color: Colors.grey[200],
                                ),
                              )
                            ],
                          ),
                        ),
                      ),
                    ),
                    Divider(),
                    SizedBox(height: 30),
                    Column(children: [
                      Text(
                        'Follow Us!',
                        style: TextStyle(fontSize: 18),
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          IconButton(
                              icon: Icon(MdiIcons.instagram, color: _iconColor),
                              onPressed: () {
                                URLLauncher.launchUrl(
                                    'https://www.instagram.com/tr3.designs');
                              }),
                          IconButton(
                              icon: Icon(MdiIcons.twitter, color: _iconColor),
                              onPressed: () {
                                URLLauncher.launchUrl(
                                    'https://twitter.com/tr3designs');
                              }),
                          IconButton(
                              icon: Icon(MdiIcons.facebook, color: _iconColor),
                              onPressed: () {
                                URLLauncher.launchUrl(
                                    'https://www.facebook.com/tr3Designs/');
                              }),
                          IconButton(
                              icon: Icon(MdiIcons.youtube, color: _iconColor),
                              onPressed: () {
                                URLLauncher.launchUrl(
                                    'https://www.youtube.com/channel/UCyMTUp7B-lFoRbDfLr3QuJw');
                              }),
                          IconButton(
                              icon: Icon(MdiIcons.pinterest, color: _iconColor),
                              onPressed: () {
                                URLLauncher.launchUrl(
                                    'https://www.pinterest.com/tr3Designs/');
                              }),
                        ],
                      )
                    ])
                  ]),
                ),
              ]),
      ),
    );
  }

  _buildProductImagesWidgets() {
    TabController imagesController =
        TabController(length: _images.length, vsync: this);
    return Padding(
      padding: const EdgeInsets.all(0.0),
      child: Container(
        height: 250.0,
        child: Center(
          child: DefaultTabController(
            length: 3,
            child: Stack(
              children: <Widget>[
                TabBarView(
                    controller: imagesController, children: _images),
                Container(
                  alignment: FractionalOffset(0.5, 0.95),
                  child: TabPageSelector(
                    controller: imagesController,
                    selectedColor: Colors.blue,
                    color: Colors.white,
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}

//Users in app, all linked to their Facebook accounts.
// _getFacebookUsers() async {
// QuerySnapshot querySnapshot= await _db.collection('Users').limit(5).getDocuments();
// querySnapshot.documents.forEach((document) async {
//   final String usersRes = await FacebookAPI.getUser(document.documentID);
//   final Map usersParsed = json.decode(usersRes);

//   User user = User();
//   user.id = usersParsed['id'];
//   user.name = usersParsed['name'];
//   user.photoUrl = usersParsed['picture']['data']['url'];

//   _users.add(user);
// });
// }

// prepareFirebaseMessaging() {
//   _firebaseMessaging = FirebaseMessaging();
//   _firebaseMessaging.configure(
//     onMessage: (Map<String, dynamic> message) {
//       print('on message $message');
//     },
//     onResume: (Map<String, dynamic> message) {
//       print('on resume $message');
//     },
//     onLaunch: (Map<String, dynamic> message) {
//       print('on launch $message');
//     },
//   );
//   _firebaseMessaging.requestNotificationPermissions(
//       const IosNotificationSettings(sound: true, badge: true, alert: true),);
//   _firebaseMessaging.getToken().then((token) {
//     if (_userId != null) {
//       _db
//           .collection('Users')
//           .document(_userId)
//           .updateData({'fcmToken': token}).catchError((e) {
//       });
//       print('FCM Token - ${token}');
//     }
//   });
// }

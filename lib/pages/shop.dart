import 'package:flutter/material.dart';
import 'package:tr3designs_flutter/services/thirdPartyAPI.dart';
import 'package:tr3designs_flutter/models/etsylisting.dart';
import 'dart:convert' show Encoding, json;
import 'package:tr3designs_flutter/pages/productDetails.dart';
import 'package:html_unescape/html_unescape.dart';
// import 'package:cached_network_image/cached_network_image.dart';

class ShopPage extends StatefulWidget {
  @override
  State createState() => ShopPageState();
}

class ShopPageState extends State<ShopPage>
    with SingleTickerProviderStateMixin {
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  final HtmlUnescape htmlUnescape = HtmlUnescape();
  final Map _listingData = Map<String, List<EtsyListing>>();

  List<Tab> _tabs = List<Tab>();
  TabController _tabController;
  bool _isLoading = true;

  @override
  void initState() {
    super.initState();

    loadPage();
  }

  //Fetch all active listings from Etsy.
  Future<Map> _getAllListingSections() async {
    String sectionsData = await EtsyAPI.getAllSections();
    return json.decode(sectionsData);
  }

  //Fetch listing data via secton id.
  Future<Map> _getListingsBySection(int sectionId) async {
    String listingsData = await EtsyAPI.getListingsBySection(sectionId);
    return json.decode(listingsData);
  }

  //Remove 'Custom', '3D', and 'Printed from title of product.
  String _removeExtrasFromTitle(String title) {
    title = title.replaceAll('Custom', '');
    title = title.replaceAll('3D', '');
    title = title.replaceAll('Printed', '');
    return title;
  }

  loadPage() async {
    //Get all sections of store.
    final Map listingSectionsData = await _getAllListingSections();

    //Create tab and listing array for all listings.
    this._tabs.add(
          Tab(text: 'All'),
        );
    List<EtsyListing> allListings = List<EtsyListing>();

    //Iterate through each section.
    for (var result in listingSectionsData['results']) {
      List<EtsyListing> listings = List<EtsyListing>();

      //Save title and section id.
      String section = result['title'];
      int shopSectionID = result['shop_section_id'];

      //Add section as tab in list.
      this._tabs.add(
            Tab(text: section),
          );

      //Get listing data.
      final Map listingsData = await _getListingsBySection(shopSectionID);

      //Iterate through each listing.
      for (var listingData in listingsData['results']) {
        EtsyListing listing = EtsyListing();

        listing.title = listingData['title'];
        listing.title = _removeExtrasFromTitle(listing.title);
        listing.url = listingData['url'];
        listing.price = listingData['price'];
        listing.photoUrl = listingData['MainImage']['url_fullxfull'];
        listing.views = listingData['views'];
        listing.description = htmlUnescape.convert(listingData['description']);
        listing.listingID = listingData['listing_id'];
        listing.num_favorers = listingData['num_favorers'];
        listing.is_customizable = listingData['is_customizable'];

        listings.add(listing);
        allListings.add(listing);
      }

      //Add listings to hashmap.
      this._listingData[section] = listings;
    }

    this._listingData['All'] = allListings;

    _tabController = TabController(vsync: this, length: this._tabs.length);

    setState(() {
      this._isLoading = false;
    });
  }

  @override
  void dispose() {
    _tabController.dispose();
    super.dispose();
  }

  Widget _buildCard(BuildContext context, EtsyListing listing) {
    final Orientation orientation = MediaQuery.of(context).orientation;

    return Card(
      color: Colors.transparent,
      elevation: 8.0,
      child: InkResponse(
        onTap: () {
          Navigator.of(context).push(
            MaterialPageRoute(
              builder: (ctx) => ProductDetailPage(listing),
            ),
          );
        },
        child: ClipRRect(
          borderRadius: BorderRadius.circular(6.0),
          child: Stack(
            children: <Widget>[
              Hero(
                tag: listing.title,
                child: Image.network(listing.photoUrl)
              ),
              Positioned(
                bottom: 0.0,
                child: Container(
                  width: orientation == Orientation.portrait
                      ? (MediaQuery.of(context).size.width - 26.0) / 2
                      : (MediaQuery.of(context).size.width - 26.0) / 4,
                  color: Colors.white.withOpacity(0.88),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      Padding(
                        padding: EdgeInsets.symmetric(vertical: 5.0),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(left: 7.0, right: 7.0),
                        child: Text(
                          listing.title,
                          style: TextStyle(
                              fontSize: 14,
                              color: Colors.black.withOpacity(0.8),
                              fontWeight: FontWeight.bold),
                          maxLines: 1,
                          overflow: TextOverflow.ellipsis,
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(top: 5.0),
                        child: Padding(
                          padding: EdgeInsets.only(left: 7.0, right: 7.0),
                          child: Text(
                            '\$${listing.price}',
                            maxLines: 1,
                            style: TextStyle(
                              fontSize: 14.0,
                              color: Colors.black.withOpacity(0.75),
                            ),
                            overflow: TextOverflow.ellipsis,
                          ),
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.symmetric(vertical: 4.0),
                      )
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          centerTitle: true,
          title: Text(
            'Shop',
            style: TextStyle(color: Colors.white),
          ),
          bottom: this._isLoading
              ? null
              : TabBar(
                  controller: _tabController,
                  isScrollable: true,
                  tabs: this._tabs,
                ),
        ),
        body: this._isLoading
            ? Center(
                child: CircularProgressIndicator(),
              )
            : TabBarView(
                controller: _tabController,
                children: this._tabs.map((Tab tab) {
                  return GridView.count(
                    physics: BouncingScrollPhysics(),
                    childAspectRatio: 0.8,
                    crossAxisCount: 2,
                    children: List.generate(this._listingData[tab.text].length,
                        (index) {
                      return _buildCard(
                          context, this._listingData[tab.text][index]);
                    }),
                  );
                }).toList(),
              ),
        key: _scaffoldKey);
  }
}

import 'package:flutter/material.dart';
import 'package:tr3designs_flutter/constants.dart';
// import 'package:cached_network_image/cached_network_image.dart';

class FilamentsPage extends StatefulWidget {
  @override
  State createState() => FilamentsPageState();
}

class FilamentsPageState extends State<FilamentsPage>
    with SingleTickerProviderStateMixin {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text('Filaments'),
      ),
      body: ListView.builder(
        itemCount: filaments.length,
        itemBuilder: (BuildContext ctxt, int index) {
          return _buildListItem(filaments[index]);
        },
      ),
    );
  }

  Widget _buildListItem(Filament filament) {
    return Column(
      children: <Widget>[
        ListTile(
          leading: CircleAvatar(
              backgroundImage: NetworkImage(filament.photoUrl)),
          subtitle: Text('MH Build Series PLA Filament - 1.75mm (1kg)'),
          title: Text(filament.name),
          trailing: IconButton(
              icon: Icon(Icons.remove_red_eye),
              onPressed: () {
                Navigator.of(context).push(
                  MaterialPageRoute(
                    builder: (ctx) => Scaffold(
                      appBar: AppBar(
                        centerTitle: true,
                        title: Text(filament.name),
                      ),
                      body: Container(
                        color: Colors.white,
                        child: GestureDetector(
                          onTap: () {
                            Navigator.pop(context);
                          },
                          child: Center(
                            child: Hero(
                              tag: filament.name,
                              child: Image.network(filament.photoUrl),
                            ),
                          ),
                        ),
                      ),
                    ),
                  ),
                );
              }),
        ),
        Divider()
      ],
    );
  }
}

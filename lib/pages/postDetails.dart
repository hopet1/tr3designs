import 'package:flutter/material.dart';
import 'package:tr3designs_flutter/services/urlLauncher.dart';

class PostDetailsPage extends StatefulWidget {
  final String _photoUrl;
  final String _imageText;
  final String _heroID;
  final String _text;
  final String _url;

  PostDetailsPage(
      this._photoUrl, this._imageText, this._heroID, this._text, this._url);

  @override
  State createState() =>
      PostDetailsPageState(_photoUrl, _imageText, _heroID, _text, _url);
}

class PostDetailsPageState extends State<PostDetailsPage>
    with TickerProviderStateMixin {
  PostDetailsPageState(
      this._photoUrl, this._imageText, this._heroID, this._text, this._url);

  final String _photoUrl;
  final String _imageText;
  final String _heroID;
  final String _text;
  final String _url;
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
          centerTitle: true, title: Text('Post Details'), actions: <Widget>[]),
      body: Container(
        color: Colors.black,
        child: Center(
          child: Hero(
            tag: this._heroID,
            child: Material(
              child: ListView(
                physics: BouncingScrollPhysics(),
                children: <Widget>[
                  _imageBox(),
                  _info(),
                  SizedBox(height: 0.0),
                ],
              ),
            ),
          ),
        ),
      ),
      bottomNavigationBar: _buildBottomNavigationBar(),
    );
  }

  _buildBottomNavigationBar() {
    return Container(
      width: MediaQuery.of(context).size.width,
      height: 50.0,
      child: RaisedButton(
        onPressed: () {
          URLLauncher.launchUrl(this._url);
        },
        color: Colors.blue,
        child: Center(
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Icon(
                Icons.open_in_browser,
                color: Colors.white,
              ),
              SizedBox(
                width: 4.0,
              ),
              Text(
                'VIEW ON INSTAGRAM',
                style: TextStyle(color: Colors.white),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget _info() {
    return Padding(
      padding: EdgeInsets.all(10),
      child: Text(this._text),
    );
  }

  Widget _imageBox() {
    return Container(
      constraints: BoxConstraints.expand(
        height: 350.0,
      ),
      padding: EdgeInsets.only(left: 16.0, bottom: 8.0, right: 16.0),
      decoration: BoxDecoration(
        image: DecorationImage(
          image: NetworkImage(this._photoUrl),
          fit: BoxFit.cover,
        ),
      ),
      child: Stack(
        children: <Widget>[
          Positioned(
            left: 0.0,
            bottom: 0.0,
            child: Container(
              child: Padding(
                padding: const EdgeInsets.all(10),
                child: Text(
                  this._imageText,
                  style: TextStyle(
                      color: Colors.white, fontWeight: FontWeight.bold),
                ),
              ),
              decoration: BoxDecoration(color: Colors.black),
              alignment: Alignment(0.0, -1.0),
            ),
          ),
          // Positioned(
          //   right: 0.0,
          //   top: 0.0,
          //   child: IconButton(
          //     icon: Icon(Icons.share),
          //     onPressed: (){
          //       Share.share(this._photoUrl);
          //     }
          //   )
          // ),
        ],
      ),
    );
  }
}

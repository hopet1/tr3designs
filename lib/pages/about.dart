import 'package:flutter/material.dart';
import 'package:tr3designs_flutter/services/thirdPartyAPI.dart';
import 'dart:convert' show json;
import 'package:tr3designs_flutter/widgets/dataRows.dart';
import 'package:tr3designs_flutter/models/user.dart';
// import 'package:get_version/get_version.dart';
import 'package:flutter/services.dart';
import 'package:tr3designs_flutter/services/urlLauncher.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:tr3designs_flutter/widgets/commonWidgets.dart' as CW;
import 'package:cloud_firestore/cloud_firestore.dart';

class AboutPage extends StatefulWidget {
  @override
  State createState() => AboutPageState();
}

class AboutPageState extends State<AboutPage> {
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  final _db = Firestore.instance;
  final Color _dividerColor = Colors.orange;
  final Color _iconColor = Colors.black;
  final String _privacyPolicyUrl =
      'https://docs.google.com/document/d/1OpE9s9AMnEXXGLyZ3n5Wfn-pBQWAVOkSRjm0AXZ5kls/edit?usp=sharing';

  bool _isLoading = true;

  String _story;
  String _policyPayment;
  String _policyShipping;
  String _policyRefunds;
  String _projectVersion;
  String _projectCode;

  List<User> _team = List<User>();

  @override
  void initState() {
    super.initState();

    getPageInfo();
  }

  _getShopInfo() async {
    String shopRes = await EtsyAPI.getShop();
    final Map shopResParsed = json.decode(shopRes);
    this._policyPayment =
        htmlUnescape.convert(shopResParsed['results'][0]['policy_payment']);
    this._policyShipping =
        htmlUnescape.convert(shopResParsed['results'][0]['policy_shipping']);
    this._policyRefunds =
        htmlUnescape.convert(shopResParsed['results'][0]['policy_refunds']);
  }

  _getShopAboutInfo() async {
    String shopAboutRes = await EtsyAPI.getShopAbout();
    final Map shopAboutResParsed = json.decode(shopAboutRes);
    this._story =
        htmlUnescape.convert(shopAboutResParsed['results'][0]['story']);
  }

  _getVersionDetails() async {
    //Version Name
    try {
      // this._projectVersion = await GetVersion.projectVersion;
    } on PlatformException {
      this._projectVersion = 'Failed to get project version.';
    }
    //Version Code
    try {
      // this._projectCode = await GetVersion.projectCode;
    } on PlatformException {
      this._projectCode = 'Failed to get build number.';
    }
  }

  _getTeam() async {
    QuerySnapshot querySnapshot = await _db
        .collection('ExtraData')
        .document('AboutPage')
        .collection('Team')
        .getDocuments();

    for (DocumentSnapshot ds in querySnapshot.documents) {
      this._team.add(
            User(ds['name'], ds['role'], ds['position'], ds['photoUrl']),
          );
    }

    //Sort team by positions value.
    this._team.sort(
          (a, b) => a.position.compareTo(b.position),
        );
  }

  getPageInfo() async {
    await _getShopInfo();
    await _getShopAboutInfo();
    await _getVersionDetails();
    await _getTeam();

    setState(() {
      this._isLoading = false;
    });
  }

  @override
  void dispose() {
    super.dispose();
    // musicPlayer.stop();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text(
          'About',
          style: TextStyle(color: Colors.white),
        ),
      ),
      body: this._isLoading
          ? Center(
              child: CircularProgressIndicator(),
            )
          : Center(
              child: Padding(
                padding: EdgeInsets.all(10),
                child: ListView(
                  physics: BouncingScrollPhysics(),
                  children: <Widget>[
                    Text(
                      'Team',
                      style: TextStyle(
                          fontSize: 32,
                          color: Colors.orange,
                          fontWeight: FontWeight.bold,
                          letterSpacing: 2.0),
                    ),
                    Padding(
                      padding: EdgeInsets.fromLTRB(0, 10, 0, 10),
                    ),
                    usersRow(this._team),
                    Divider(color: _dividerColor),
                    CW.listTile(
                        MdiIcons.bookOpen, _iconColor, 'STORY', this._story),
                    Divider(color: _dividerColor),
                    CW.listTile(MdiIcons.contactlessPayment, _iconColor,
                        'PAYMENT POLICY', this._policyPayment),
                    Divider(color: _dividerColor),
                    CW.listTile(MdiIcons.mailbox, _iconColor, 'SHIPPING POLICY',
                        this._policyShipping),
                    Divider(color: _dividerColor),
                    CW.listTile(MdiIcons.homeCurrencyUsd, _iconColor,
                        'REFUNDS POLICY', this._policyRefunds),
                    Divider(color: _dividerColor),
                    CW.listTile(MdiIcons.information, _iconColor, 'CREDITS',
                        'Icons for the introduction slides and background for the login page made by Freepik from www.flaticon.com. This app was developed by Tr3umphant.Designs, LLC.'),
                    Divider(color: _dividerColor),
                    CW.listTile(MdiIcons.cellphoneIphone, _iconColor, 'VERSION',
                        '${this._projectVersion}'),
                    Divider(color: _dividerColor),
                    CW.listTileWithAction(MdiIcons.security, _iconColor,
                        'PRIVACY POLICY', 'View Here', () {
                      URLLauncher.launchUrl(this._privacyPolicyUrl);
                    }),
                  ],
                ),
              ),
            ),
    );
  }
}

// import 'package:flutter/material.dart';
// import 'package:shared_preferences/shared_preferences.dart';
// import 'package:cloud_firestore/cloud_firestore.dart';
// import 'package:tr3designs_flutter/services/modal.dart';
// import 'package:tr3designs_flutter/services/notification.dart';
// import 'package:tr3designs_flutter/constants.dart';

// class NotificationsPage extends StatefulWidget {
//   @override
//   State createState() => NotificationsPageState();
// }

// class NotificationsPageState extends State<NotificationsPage>
//     with SingleTickerProviderStateMixin {
//   final TextStyle heading = TextStyle(fontSize: 18.0);
//   final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
//   final _db = Firestore.instance;

//   bool _isLoading = true;
//   bool _nfNewProduct;
//   bool _nfWeeklyGiveaway;

//   String _id;

//   @override
//   void initState() {
//     super.initState();

//     getSharedPreferences();
//   }

//   getSharedPreferences() async {
//     SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
//     this._id = sharedPreferences.getString('id');
//     DocumentSnapshot documentSnapshot =
//         await _db.collection('Users').document(this._id).get();
//     this._nfNewProduct = documentSnapshot['nfNewProduct'];
//     this._nfWeeklyGiveaway = documentSnapshot['nfWeeklyGiveaway'];

//     setState(() {
//       this._isLoading = false;
//     });
//   }

//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//         appBar: AppBar(centerTitle: true, title: Text('Notifications')),
//         body: this._isLoading
//             ? Center(child: CircularProgressIndicator())
//             : ListView(children: <Widget>[
//                 //Product
//                 Container(
//                     child: ListTile(
//                       title: Text('Product', style: heading),
//                       subtitle: Text('A new product was added.'),
//                       trailing: Switch(
//                           value: this._nfNewProduct,
//                           onChanged: (bool value) {
//                             setState(() {
//                               value
//                                   ? MyNotification()
//                                       .subscribeToTopic(TOPIC_NEW_PRODUCT)
//                                   : MyNotification()
//                                       .unsubscribeFromTopic(TOPIC_NEW_PRODUCT);
//                               this._nfNewProduct = value;
//                               _db
//                                   .collection('Users')
//                                   .document(this._id)
//                                   .updateData({
//                                 'nfNewProduct': this._nfNewProduct
//                               }).catchError((e) {
//                                 print(e.toString());
//                               });
//                             });
//                           }),
//                     ),
//                     decoration: BoxDecoration(
//                         border:
//                             Border(bottom: BorderSide(color: Colors.grey)))),
//                 //Weekly Giveaway
//                 Container(
//                     child: ListTile(
//                       title: Text('Weekly Giveaway', style: heading),
//                       subtitle: Text('The new contest has started.'),
//                       trailing: Switch(
//                           value: this._nfWeeklyGiveaway,
//                           onChanged: (bool value) {
//                             setState(() {
//                               value
//                                   ? MyNotification()
//                                       .subscribeToTopic(TOPIC_WEEKLY_GIVEAWAY)
//                                   : MyNotification().unsubscribeFromTopic(
//                                       TOPIC_WEEKLY_GIVEAWAY);
//                               this._nfWeeklyGiveaway = value;
//                               _db
//                                   .collection('Users')
//                                   .document(this._id)
//                                   .updateData({
//                                 'nfWeeklyGiveaway': this._nfWeeklyGiveaway
//                               }).catchError((e) {
//                                 print(e.toString());
//                               });
//                             });
//                           }),
//                     ),
//                     decoration: BoxDecoration(
//                         border:
//                             Border(bottom: BorderSide(color: Colors.grey)))),
//               ]));
//   }
// }

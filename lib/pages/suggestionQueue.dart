import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
// import 'package:flutter_app_badger/flutter_app_badger.dart';
import 'package:tr3designs_flutter/services/modal.dart';

class SuggestionQueuePage extends StatefulWidget {
  @override
  State createState() => SuggestionQueuePageState();
}

class SuggestionQueuePageState extends State<SuggestionQueuePage>
    with SingleTickerProviderStateMixin {
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  final _db = Firestore.instance;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text('Suggestion Queue'),
      ),
      body: StreamBuilder<QuerySnapshot>(
        stream: _db
            .collection('Suggestions')
            .where('safe', isEqualTo: false)
            .snapshots(),
        builder: (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot) {
          if (snapshot.hasError) return Text('Error: ${snapshot.error}');
          switch (snapshot.connectionState) {
            case ConnectionState.waiting:
              return Center(
                child: Text('Loading...'),
              );
            default:
              if (snapshot.data.documents.isEmpty) {
                return Center(
                  child: Text(
                    'No suggestions...',
                    style: TextStyle(fontSize: 18),
                  ),
                );
              } else {
                return ListView.builder(
                    itemCount: snapshot.data.documents.length,
                    itemBuilder: (BuildContext context, int i) {
                      return _build(snapshot.data.documents[i]);
                    });
              }
          }
        },
      ),
      key: _scaffoldKey,
    );
  }

  void approve(DocumentSnapshot document) {
    final DocumentReference postRef =
        _db.collection('Suggestions').document(document['id']);
    postRef.updateData({'safe': true}).then((res) {}).catchError((e) {
          Modal.showAlert(
            context,
            'Error',
            e.toString(),
          );
        });
  }

  void delete(DocumentSnapshot document) {
    final DocumentReference postRef =
        _db.collection('Suggestions').document(document['id']);
    postRef.delete().then((res) {}).catchError((e) {
      Modal.showAlert(
        context,
        'Error',
        e.toString(),
      );
    });
  }

  Widget _build(DocumentSnapshot document) {
    return Card(
      child: Row(
        children: <Widget>[
          Expanded(
            child: Container(
              child: ListTile(
                leading: IconButton(
                    icon: Icon(Icons.lock_open),
                    onPressed: () {
                      showDialog(
                        barrierDismissible: false,
                        builder: (_) => AlertDialog(
                          title: Text('Approve Suggestion?'),
                          content: Text(document['description']),
                          actions: <Widget>[
                            FlatButton(
                              onPressed: () {
                                Navigator.pop(context);
                              },
                              child: Text('Close'),
                            ),
                            FlatButton(
                              onPressed: () {
                                approve(document);
                                Navigator.pop(context);
                              },
                              child: Text('Approve'),
                            ),
                          ],
                        ),
                        context: context,
                      );
                    }),
                title: Text(
                  document['description'],
                  style: TextStyle(fontSize: 18.0),
                ),
                trailing: IconButton(
                    icon: Icon(Icons.delete),
                    onPressed: () {
                      showDialog(
                        barrierDismissible: false,
                        builder: (_) => AlertDialog(
                          title: Text('Delete Suggestion?'),
                          content: Text(document['description']),
                          actions: <Widget>[
                            FlatButton(
                              onPressed: () {
                                Navigator.pop(context);
                              },
                              child: Text('Close'),
                            ),
                            FlatButton(
                              onPressed: () {
                                delete(document);
                                Navigator.pop(context);
                              },
                              child: Text('Delete'),
                            ),
                          ],
                        ),
                        context: context,
                      );
                    }),
              ),
            ),
          )
        ],
      ),
    );
  }
}

import 'package:flutter/material.dart';
import 'package:flutter_facebook_login/flutter_facebook_login.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:tr3designs_flutter/services/urlLauncher.dart';
import 'package:tr3designs_flutter/constants.dart';

class SettingsPage extends StatefulWidget {
  @override
  State createState() => SettingsPageState();
}

class SettingsPageState extends State<SettingsPage> {
  final FacebookLogin _facebookLogin = FacebookLogin();
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  final Color _iconColor = Colors.black;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text('Settings'),
      ),
      body: ListView(
        physics: BouncingScrollPhysics(),
        children: <Widget>[
          // ListTile(
          //     onTap: () async {
          //       Navigator.push(
          //           context,
          //           MaterialPageRoute(
          //               builder: (context) => NotificationsPage(),),);
          //     },
          //     title: Text('Notifications'),
          //     leading: Icon(MdiIcons.messageAlert, color: this._iconColor),),
          // Divider(),
          ListTile(
            onTap: () {
              URLLauncher.launchUrl(MAILING_LIST_URL);
            },
            title: Text('Mailing List'),
            leading: Icon(MdiIcons.email, color: this._iconColor),
          ),
          Divider(),
          // ListTile(
          //     onTap: () async {
          //       showDialog(
          //         barrierDismissible: false,
          //         builder: (_) => AlertDialog(
          //           title: Text('Logout'),
          //           actions: <Widget>[
          //             //Cancel logout.
          //             FlatButton(
          //                 onPressed: () {
          //                   Navigator.pop(context);
          //                 },
          //                 child: Text(
          //                   'Cancel',
          //                 ),),
          //             //Confirm logout.
          //             FlatButton(
          //                 onPressed: () async {
          //                   Navigator.pop(context);

          //                   await _facebookLogin.logOut();
          //                   //SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
          //                   //await sharedPreferences.clear();

          //                   Navigator.of(context)
          //                       .popUntil((route) => route.isFirst);
          //                 },
          //                 child: Text('Yes',
          //                     ),),
          //           ],
          //         ),
          //         context: context,
          //       );
          //     },
          //     title: Text('Logout'),
          //     leading: Icon(Icons.exit_to_app, color: this._iconColor),),
          // Divider()
        ],
      ),
    );
  }
}

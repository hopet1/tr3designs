import 'package:flutter/material.dart';
import 'package:tr3designs_flutter/constants.dart';
import 'package:tr3designs_flutter/pages/newSuggestion.dart';
import 'package:tr3designs_flutter/pages/suggestionQueue.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
// import 'package:device_id/device_id.dart';
import 'package:timeago/timeago.dart' as timeago;
import 'package:tr3designs_flutter/services/animator.dart';

class SuggestionsPage extends StatefulWidget {
  @override
  State createState() => SuggestionsPageState();
}

class SuggestionsPageState extends State<SuggestionsPage> {
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  final int charLimit = 30;
  final _db = Firestore.instance;
  bool _isLoading = true;
  // String _deviceId;

  @override
  void initState() {
    super.initState();

    loadPage();
  }

  loadPage() async {
    //Get device id.
    // _deviceId = await DeviceId.getID;

    setState(() {
      this._isLoading = false;
    });
  }

  favorite(DocumentSnapshot ds) async {
    final DocumentReference postRef =
        _db.collection('Suggestions').document(ds.documentID);
    List<dynamic> users = List.from(ds.data['users']);

    // if (users.contains(_deviceId)) {
    //   users.remove(_deviceId);

    //   _db
    //       .collection('Suggestions')
    //       .document(ds.documentID)
    //       .updateData({'likes': ds.data['likes'] - 1, 'users': users});
    // } else {
    //   users.add(_deviceId);
    //   _db
    //       .collection('Suggestions')
    //       .document(ds.documentID)
    //       .updateData({'likes': ds.data['likes'] + 1, 'users': users});
    // }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          centerTitle: true,
          title: Text('Suggestions', style: TextStyle(color: Colors.white),),
        ),
        bottomNavigationBar: _buildBottomNavigationBar(),
        body: this._isLoading
            ? Center(child: CircularProgressIndicator(),)
            : Container(
                child: CustomScrollView(
                physics: BouncingScrollPhysics(),
                shrinkWrap: true,
                slivers: <Widget>[
                  SliverToBoxAdapter(
                    child: Padding(
                      padding: const EdgeInsets.all(30),
                      child: Center(
                        child: Text(
                          'CREATE AND LIKE NEW IDEAS.',
                          style: TextStyle(
                              fontWeight: FontWeight.bold,
                              fontSize: 14.0,
                              letterSpacing: 2.0,
                              color: Colors.grey),
                        ),
                      ),
                    ),
                  ),
                  StreamBuilder<QuerySnapshot>(
                    stream: _db
                        .collection('Suggestions')
                        .where('safe', isEqualTo: true)
                        .snapshots(),
                    builder: (BuildContext context,
                        AsyncSnapshot<QuerySnapshot> snapshot) {
                      return SliverList(
                          delegate: SliverChildBuilderDelegate(
                        (context, index) {
                          return _buildListItem(snapshot.data.documents[index]);
                        },
                        childCount: snapshot.hasData
                            ? snapshot.data.documents.length
                            : 0,
                      ),);
                    },
                  )
                ],
              ),),
        key: _scaffoldKey);
  }

  Widget _buildListItem(DocumentSnapshot ds) {
    return Padding(
      padding: EdgeInsets.fromLTRB(20, 0, 20, 0),
      child: Card(
        child: Padding(
          padding: EdgeInsets.only(
            left: 10.0,
            top: 10.0,
            right: 10.0,
          ),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Row(
                children: <Widget>[
                  Padding(
                    padding: EdgeInsets.only(
                      right: 10.0,
                    ),
                    child: ClipRRect(
                      borderRadius: BorderRadius.circular(65.0),
                      child: Image.asset('assets/images/logo.png',
                          width: 55, height: 55),
                    ),
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text(
                        '\"${ds['description']}\"',
                        maxLines: 2,
                        // overflow: TextOverflow.ellipsis,
                        style: TextStyle(
                          color: Colors.lightGreen,
                          fontSize: 15.0,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                      Padding(
                          padding: EdgeInsets.only(
                            top: 5.0,
                            bottom: 5.0,
                          ),
                          child: Text('Anonymous',
                              style: TextStyle(
                                  fontSize: 14.0, letterSpacing: 2.0),),)
                    ],
                  ),
                ],
              ),
              ButtonTheme.bar(
                padding: EdgeInsets.all(2.0),
                child: ButtonBar(
                  alignment: MainAxisAlignment.spaceEvenly,
                  children: <Widget>[
                    Text(
                      timeago.format(ds['time'].toDate(),),
                      style: TextStyle(fontSize: 14.0),
                      textAlign: TextAlign.center,
                    ),
                    Text('${ds['likes']} ' +
                        (ds['likes'] == 1 ? 'like' : 'likes'),),
                    // Animations.fastOutSlowIn(
                    //     IconButton(
                    //       icon: List.from(ds['users']).contains(_deviceId)
                    //           ? Icon(Icons.favorite, color: Colors.red)
                    //           : Icon(Icons.favorite_border),
                    //       onPressed: () {
                    //         favorite(ds);
                    //       },
                    //     ),
                    //     4),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  _buildBottomNavigationBar() {
    return Container(
      width: MediaQuery.of(context).size.width,
      height: 50.0,
      child: RaisedButton(
        onPressed: () {
          // print('Device ID: ${_deviceId}');
          // if (_deviceId == ADMIN_DEVICE_ID) {
          //   Navigator.push(
          //     context,
          //     MaterialPageRoute(
          //       builder: (context) => SuggestionQueuePage(),
          //     ),
          //   );
          // } else {
          //   Navigator.push(
          //     context,
          //     MaterialPageRoute(
          //       builder: (context) => NewSuggestionPage(),
          //     ),
          //   );
          // }
        },
        color: Colors.blue,
        child: Center(
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Icon(
                Icons.note,
                color: Colors.white,
              ),
              SizedBox(
                width: 4.0,
              ),
              Text(
                'CREATE NEW SUGGESTION',
                style: TextStyle(color: Colors.white),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

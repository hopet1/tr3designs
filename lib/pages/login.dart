// import 'package:flutter/material.dart';
// import 'package:tr3designs_flutter/pages/home.dart';
// import 'package:tr3designs_flutter/services/modal.dart';
// import 'package:tr3designs_flutter/services/device.dart';
// import 'package:tr3designs_flutter/services/animator.dart';
// import 'package:shared_preferences/shared_preferences.dart';
// import 'package:flutter_facebook_login/flutter_facebook_login.dart';
// import 'package:http/http.dart' as http;
// import 'dart:convert' show Encoding, json;
// import 'package:tr3designs_flutter/constants.dart';
// import 'package:intro_views_flutter/intro_views_flutter.dart';
// import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';

// class LoginPage extends StatefulWidget {
//   @override
//   State createState() => LoginPageState();
// }

// class LoginPageState extends State<LoginPage>
//     with SingleTickerProviderStateMixin {
//   final FacebookLogin _facebookLogin = FacebookLogin();

//   @override
//   void initState() {
//     super.initState();

//     checkLoggedIn();
//   }

//   checkLoggedIn() async {
//     bool isLoggedIn = await _facebookLogin.isLoggedIn;
//     if (isLoggedIn) {
//       Navigator.push(
//         context,
//         MaterialPageRoute(builder: (context) => HomePage()),
//       );
//     }
//   }

//   loginWithFacebook() async {
//     final result = await _facebookLogin.logInWithReadPermissions(['email']);

//     switch (result.status) {
//       case FacebookLoginStatus.loggedIn:
//         try {
//           final token = result.accessToken.token;
//           print('Token - ${token}');
//           final graphResponse = await http.get(
//               'https://graph.facebook.com/v2.12/me?fields=name,picture.type(large),email&access_token=${token}');
//           final profile = json.decode(graphResponse.body);

//           print(profile);

//           final String name = profile['name'];
//           final String email = profile['email'];
//           final String id = profile['id'];
//           final String photoUrl = profile['picture']['data']['url'];

//           //Store facebook profile information to shared preferenes.
//           SharedPreferences sharedPreferences =
//               await SharedPreferences.getInstance();
//           await sharedPreferences.setString('id', id);
//           await sharedPreferences.setString('photoUrl', photoUrl);
//           await sharedPreferences.setString('displayName', name);
//           await sharedPreferences.setString('email', email);

//           //Detect if user has entered the app from this device before.
//           bool welcome = sharedPreferences.getBool('welcome') != null;

//           //Store if user is admin or not.
//           await sharedPreferences.setBool(
//               'isAdmin', id == ADMIN_FACEBOOK_ID && Device.isAndroid());

//           //Make sure facebook info is valid before entering app.
//           if (name == null || id == null || email == null || photoUrl == null) {
//             Modal.showAlert(
//                 context, 'Error', 'Could not save facebook info. Try again.');
//           } else {
//             Navigator.push(
//               context,
//               MaterialPageRoute(
//                   builder: (context) => welcome
//                       ? HomePage()
//                       : IntroViewsFlutter(
//                           INTRO_PAGES,
//                           onTapDoneButton: () {
//                             Navigator.push(
//                               context,
//                               MaterialPageRoute(
//                                 builder: (context) => HomePage(),
//                               ), //MaterialPageRoute
//                             );
//                           },
//                           showSkipButton:
//                               true, //Whether you want to show the skip button or not.
//                           pageButtonTextStyles: TextStyle(
//                             color: Colors.white,
//                             fontSize: 18.0,
//                           ),
//                         )),
//             );
//           }
//         } catch (e) {
//           print(e.toString());
//           Modal.showAlert(
//               context, 'Error', FacebookLoginStatus.error.toString());
//         }
//         break;
//       case FacebookLoginStatus.cancelledByUser:
//         print('Cancelled By User');
//         break;
//       case FacebookLoginStatus.error:
//         Modal.showAlert(context, 'Error', FacebookLoginStatus.error.toString());
//         break;
//     }
//   }

//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//         backgroundColor: Colors.lightGreen,
//         body: Container(
//           // color: Colors.white,
//           decoration: new BoxDecoration(
//             image: new DecorationImage(
//               image: new AssetImage('assets/images/background.jpg'),
//               fit: BoxFit.cover,
//             ),
//           ),
//           child: Center(
//             child: ListView(
//               shrinkWrap: true,
//               padding: EdgeInsets.only(left: 50.0, right: 50.0),
//               children: <Widget>[
//                 Animations.opacity(Hero(
//                   tag: 'hero',
//                   child: CircleAvatar(
//                     backgroundColor: Colors.transparent,
//                     radius: 80.0,
//                     child: Image.asset('assets/images/logo.png'),
//                   ),
//                 )),
//                 SizedBox(height: 48.0),
//                 Padding(
//                     padding: EdgeInsets.symmetric(vertical: 16.0),
//                     child: RaisedButton.icon(
//                         color: Colors.blue,
//                         onPressed: () {
//                           this.loginWithFacebook();
//                         },
//                         icon: Icon(MdiIcons.facebook, color: Colors.white),
//                         label: Text('Facebook Login',
//                             style: TextStyle(
//                                 color: Colors.white,
//                                 letterSpacing: 2.0,
//                                 fontWeight: FontWeight.bold)))
//                     // child: RaisedButton(
//                     //   shape: RoundedRectangleBorder(
//                     //     borderRadius: BorderRadius.circular(24),
//                     //   ),
//                     //   onPressed: () {
//                     //     this.loginWithFacebook();
//                     //   },
//                     //   padding: EdgeInsets.all(12),
//                     //   color: Colors.blue,
//                     //   child: Text('Facebook Login', style: TextStyle(color: Colors.white)),
//                     // ),
//                     )
//               ],
//             ),
//           ),
//         ));
//   }
// }

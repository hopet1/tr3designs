import 'package:flutter/material.dart';
import 'package:tr3designs_flutter/pages/home.dart';
import 'package:tr3designs_flutter/pages/shop.dart';
import 'package:tr3designs_flutter/pages/suggestions.dart';
import 'package:tr3designs_flutter/pages/settings.dart';

class Views extends StatefulWidget {
  @override
  State createState() => ViewsState();
}

class ViewsState extends State<Views> {
  int _currentIndex = 0;
  List<Widget> _pages;

  static Color _activeColor = Colors.orange;
  static Color _inactiveColor = Colors.white;

  //Home
  BottomNavigationBarItem _homeActive = BottomNavigationBarItem(
    icon: Icon(Icons.home, color: _activeColor),
    title: Text(
      'Home',
      style: TextStyle(color: _activeColor),
    ),
  );
  BottomNavigationBarItem _homeInactive = BottomNavigationBarItem(
    icon: Icon(Icons.home, color: _inactiveColor),
    title: Text(
      'Home',
      style: TextStyle(color: _inactiveColor),
    ),
  );

  //Suggestions
  BottomNavigationBarItem _suggestionsActive = BottomNavigationBarItem(
    icon: Icon(Icons.lightbulb_outline, color: _activeColor),
    title: Text(
      'Suggestions',
      style: TextStyle(color: _activeColor),
    ),
  );
  BottomNavigationBarItem _suggestionsInactive = BottomNavigationBarItem(
    icon: Icon(Icons.lightbulb_outline, color: _inactiveColor),
    title: Text(
      'Suggestions',
      style: TextStyle(color: _inactiveColor),
    ),
  );

  //Shop
  BottomNavigationBarItem _shopActive = BottomNavigationBarItem(
    icon: Icon(Icons.shopping_cart, color: _activeColor),
    title: Text(
      'Shop',
      style: TextStyle(color: _activeColor),
    ),
  );
  BottomNavigationBarItem _shopInactive = BottomNavigationBarItem(
    icon: Icon(Icons.shopping_cart, color: _inactiveColor),
    title: Text(
      'Shop',
      style: TextStyle(color: _inactiveColor),
    ),
  );

  //Settings
  BottomNavigationBarItem _settingsActive = BottomNavigationBarItem(
    icon: Icon(Icons.settings, color: _activeColor),
    title: Text(
      'Settings',
      style: TextStyle(color: _activeColor),
    ),
  );
  BottomNavigationBarItem _settingsInactive = BottomNavigationBarItem(
    icon: Icon(Icons.settings, color: _inactiveColor),
    title: Text(
      'Settings',
      style: TextStyle(color: _inactiveColor),
    ),
  );

  @override
  void initState() {
    super.initState();

    this._pages = [HomePage(), SuggestionsPage(), ShopPage(), SettingsPage()];
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        automaticallyImplyLeading: false,
        title: Text('tr3Designs'),
      ),
      body: IndexedStack(index: _currentIndex, children: this._pages),
      bottomNavigationBar: BottomNavigationBar(
        currentIndex: _currentIndex,
        items: [
          this._currentIndex == 0 ? this._homeActive : this._homeInactive,
          this._currentIndex == 1
              ? this._suggestionsActive
              : this._suggestionsInactive,
          this._currentIndex == 2 ? this._shopActive : this._shopInactive,
          this._currentIndex == 3
              ? this._settingsActive
              : this._settingsInactive,
        ],
        onTap: (int index) {
          setState(() {
            _currentIndex = index;
          });
        },
      ),
    );
  }
}

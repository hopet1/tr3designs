class User {
  String name;
  String role;
  int position;
  String photoUrl;

  User(name, role, position, photoUrl){
    this.name = name;
    this.role = role;
    this.position = position;
    this.photoUrl = photoUrl;
  }
}

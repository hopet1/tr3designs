// import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';

class EtsyListing {
  String url;
  String price;
  String title;
  int views;
  String photoUrl;
  String description;
  int listingID;
  int num_favorers;
  bool is_customizable;
  List<Image> images = List<Image>();
}

class YouTubeVideo {
  String id;
  String title;
  DateTime publishedAt;
  String thumbnailUrl;
  String description;
}

class BloggerPost {
  String id;
  String url;
  String title;
  String content;
  DateTime published;
  String imageUrl;
}

class Suggestion {
  String id;
  String description;
  int likes;
  List<String> users;
  String user;
  String photoUrl;
  String displayName;
  bool safe;
  DateTime time;

  Suggestion(String photoUrl, String description, String displayName, DateTime time, int likes, List<String> users, String id){
    this.photoUrl = photoUrl;
    this.description = description;
    this.displayName = displayName;
    this.time = time;
    this.likes = likes;
    this.users = users;
    this.id = id;
  }
}
